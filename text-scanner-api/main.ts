import express, { Request, Response } from "express";
import bodyParser from "body-parser";
import Knex from "knex";
import knexConfigs from "./knexfile";
import fs from "fs";
import cors from "cors";
import multer from "multer";
import path from "path";
import dotenv from "dotenv";
import { reqLogger } from "./utils/logger";
import { awsStorage } from "./utils/image.io";
// import fetch from "node-fetch";
dotenv.config();

const mode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[mode];
export const knex = Knex(knexConfig);
export const env = dotenv.parse(fs.readFileSync(".env." + mode).toString());

const app = express();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    },
});

const summaryStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/../image_storage`);
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    },
});

const cardStorage = multer.diskStorage({
    destination: '../image_storage',
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    }
})

const letterStorage = multer.diskStorage({
    destination: '../image_storage',
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-letter${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    }
})

export const upload = multer({ storage: storage });
export const summaryUpload = multer({ storage: summaryStorage });// store to a private file that holds all private image
export const cardUpload = multer({ storage: cardStorage });
export const awsUpload = multer({ storage: awsStorage });
export const letterUpload = multer({ storage: letterStorage })

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(reqLogger);

import { UserService } from "./services/users.service";
import { CardService } from "./services/card.service";
import { ProfileService } from "./services/profile.service"
import { ProfileController } from "./controllers/profile.controller"
import { TextSummaryService } from "./services/textSummary.service";
import { UserController } from "./controllers/users.controller";
import { CardController } from "./controllers/card.controller";
import { TextSummaryController } from "./controllers/textSummary.controller";
import { LetterController } from "./controllers/letter.controller"
import { LetterService } from "./services/letter.service"
import { createIsLoggedIn } from "./utils/guards";
import { ItemController } from "./controllers/item.controller";
import { ItemService } from "./services/item.service";

const userService = new UserService(knex);
const cardService = new CardService(knex);
const profileService = new ProfileService(knex);
const textSummaryService = new TextSummaryService(knex)
const letterService = new LetterService(knex)
const itemService = new ItemService(knex)

export const userController = new UserController(userService);
export const isLoggedIn = createIsLoggedIn(userService);
export const cardController = new CardController(cardService);
export const profileController = new ProfileController(profileService)
export const textSummaryController = new TextSummaryController(textSummaryService)
export const letterController = new LetterController(letterService)
export const itemController = new ItemController(itemService)
// app.get("/test", userController.getAllUsers)

app.use(express.static("public"));
app.use(express.static("uploads"));
app.use(express.static(path.join(__dirname, '../image_storage/')))
import { routes } from "./routes";

// const API_VERSION = "/api/v1";
const API_VERSION = "/";

app.use(API_VERSION, routes);

// catch-all route not found error middleware
const routeNotFound = (req: Request, res: Response) => {
    res.status(404).json({ error: 'API Route does not exist.' })
}
app.use(routeNotFound);

const PORT = 3030;

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});
