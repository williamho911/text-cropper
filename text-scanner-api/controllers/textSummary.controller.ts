// app.post('/uploadimg', privateUpload.single('image'), (req, res) => {
//     console.log(req.file); // get the name of the file
//     console.log(req.body); // Other fields are in req.body
//     //fetch python and get OCR Text to be written
//     // pack the OCR Text to the data
//     // Store the data to database and pass the id to the object 
//     res.json({ success: true, data: { OCRText: "I am peter chan" } })
//     // res.json({success:false, data:{text:"I am peter chan"}})
// })

// app.post('/generatesummary', async (req, res) => {
//     //get the confirmed text
//     console.log(123456, req.body)
//     // fetch string to python and get summary
//     let text = req.body.Text;
//     console.log("text",text);

//     let ratio = 0.1;

//     let result = await fetch(process.env.PYTHON_SERVER+"/document/textsummary",
//         {
//             headers: {"Content-Type":"application/json"},
//             method: 'POST',
//             body: JSON.stringify({text,ratio})
//         });

//     let json = await result.json();
//     console.log("textsummary json",json);

//     res.json({ success: true, data: { json } })
// })

// app.post('/savesummary', (req, res) => {
//     console.log(req.body)
//     // save to PSQL / server 
//     res.json({ success: true, status: "Redirect" })
// })
import { Response, Request } from "express"
import fetch from "node-fetch";
import { TextSummaryService } from "../services/textSummary.service";


export class TextSummaryController {
    constructor(private textSummaryService: TextSummaryService) { }



    getOCRTextFromImg = async (req: Request, res: Response) => {
        //@ts-ignore
        const fileName = req.file.key
        // const userId = parseInt(req.body?.id)
        // const filePath = __dirname + `../../image_storage/${fileName}`
        // const itemId = this.textSummaryService.saveItem(fileName, userId)
        // console.log(itemId)
        const itemId = await this.textSummaryService.saveItem(fileName, req.user?.id as number)
        console.log(itemId)
        // const fetchPath = "http://localhost:5000/document/ocr" // put the fetch path here 
        const fetchPath = process.env.PYTHON_SERVER + "/document/ocr" // put the fetch path here 
        try {
            const result = await fetch(fetchPath, {
                method: "POST",

                headers: {
                    'Content-Type': 'application/json'
                },

                body: JSON.stringify({ original_photo_url: fileName }),
            })
            const OCRReturn = await result.json()
            const OCRResult = OCRReturn.text
            console.log(OCRResult)
            // Store the data to database and pass the id to the object 
            res.json({ success: true, itemId, data: { OCRResult: OCRResult } })
            // res.json({success:false, itemId:itemId, data:{OCRResult:"I am peter chan"}})
            return

        } catch (e) {
            return res.json({ success: false, error: "Failed to use OCR service" })
        }
    }

    generateSummary = async (req: Request, res: Response) => {
        //get the confirmed text
        console.log("generateSummaryCalled", req.body)
        // fetch string to python and get summary
        let text = req.body.Text;
        let itemId = req.body.itemId
        let Mode = req.body.Mode
        let userId = req.user?.id
        console.log("Mode", Mode)
        let documentId
        if (Mode === 1) {
            itemId = await this.textSummaryService.saveItem(undefined, userId as number)
        }
        documentId = await this.textSummaryService.saveDocumentOCRText(itemId, text)
        console.log("documetId", documentId)
        try {
            if (req.body.summaryMode == 2) {
                let Percentage = parseInt(req.body.Percentage)
                let ratio = 0.2
                if (Percentage > 100) {
                    ratio = 1
                } else if (Percentage <= 0) {
                    ratio = 0.2
                } else {
                    ratio = Percentage / 100
                }
                // let result = await fetch("http://localhost:5000/document/textsummary",
                let result = await fetch(process.env.PYTHON_SERVER + "/document/textsummary",
                    {
                        headers: { "Content-Type": "application/json" },
                        method: 'POST',
                        body: JSON.stringify({ text, ratio })
                    });

                let textSummary = await result.json();
                console.log(textSummary.summarizedText)
                if (textSummary.error) {
                    res.json({ success: false, error: textSummary.error })
                } else {
                    res.json({ success: true, itemId, documentId, data: { textSummary } })
                }
                return

            } else {
                // let result = await fetch("http://localhost:5000/document/textabstractivesummary",
                let result = await fetch(process.env.PYTHON_SERVER + "/document/textabstractivesummary",
                    {
                        headers: { "Content-Type": "application/json" },
                        method: 'POST',
                        body: JSON.stringify({ text })
                    });

                let textSummary = await result.json();
                if (textSummary.error) {
                    res.json({ success: false, error: textSummary.error })
                } else {
                    res.json({ success: true, itemId, documentId, data: { textSummary } })
                }
                return
            }

        } catch (e) {
            return res.json({ success: false, error: "Failed to use Summary service" })
        }
        // res.json({ success: true, itemId, data: { textSummary: "123456789" } })
    }

    saveSummary = async (req: Request, res: Response) => {
        console.log(req.body)
        // const summaryText = req.body.Text
        const item_id = req.body?.itemId
        const document_id = req.body?.documentId
        const summarized_text = req.body?.Text
        console.log(item_id, document_id, summarized_text)
        const result = await this.textSummaryService.saveSummary(item_id, document_id, summarized_text)
        console.log(result)
        console.log("123Result")
        // save to PSQL / server 
        res.json({ success: true, status: "Redirect" })
    }

    getRecord = async (req: Request, res: Response) => {
        console.log('Getting Records')
        let results: any = []
        try {
            const sqlresults = await this.textSummaryService.getDocuments(req.user?.id as number)
            if (sqlresults.length > 0) {
                for (let result of sqlresults) {
                    // results.push({...result, created_at: result.created_at.toLocaleDateString("en-US",options)})
                    results.push({ ...result, created_at: result.created_at.toDateString() })
                }
            }
            console.log(results)

        } catch (error) {
            res.status(500).json("Internal Server Error")
        }
        res.json({ success: true, allTextSummary: results })
    }

}
