import { Response, Request } from "express"
import { ProfileService } from "../services/profile.service"
import { hashPassword } from "../utils/hash";
import { getTimeLimitedImageUrl } from "../utils/image.io";

export class ProfileController {
    constructor(private profileService: ProfileService) {}

    getProfile = async (req: Request, res: Response) => {
        console.log("Getting profile");
        
        try {
            const profile = await this.profileService.getProfile(req.user?.id as number)
            profile.profile_picture_url = getTimeLimitedImageUrl(profile.profile_picture_url)
            console.log("get user profile from db:", profile);
            
            res.json({profile})
            
        } catch (error) {
            res.status(500).json({ success: false, message: "Internal server error" });
        }
    }

    postProfilePhoto = async (req: Request, res: Response) => {
        console.log('Receiving photo')
        console.log(req.file.filename);
        try {
            const result = await this.profileService.saveImage(req.user?.id as number, req.file.filename)
            console.log(result)
            res.json(result)
            return
        } catch (err) {
            res.json({msg: 'Internal server error'})
        }
    }

    postName = async (req: Request, res: Response) => {
        console.log('Receiving Name')
        console.log(req.body.name);
        try {
            const result = await this.profileService.saveName(req.user?.id as number, req.body.name)
            console.log(result)
            res.json(result)
            return
        } catch (error) {
            res.json({msg: 'Internal server error'})
        }
    }

    postPassword = async (req: Request, res: Response) => {
        console.log('Receiving Password')
        const { password } = req.body
        const hashedPassword = await hashPassword(password.toString());        
        try {
            const result = await this.profileService.savePassword(req.user?.id as number, hashedPassword)
            console.log(result)
            res.json(result)
            return
        } catch (error) {
            res.json({msg: 'Internal server error'})
        }
    }
}