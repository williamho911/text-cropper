import { UserService } from "../services/users.service";
import { UserController } from "./users.controller";
import { Request, Response } from "express";
import Knex from "knex";

describe("User Controller Testsuit", () => {
    let userController: UserController;
    let userService: UserService;
    let resJson: jest.SpyInstance;
    let req: Request;
    let res: Response;

    beforeEach(() => {
        userService = new UserService({} as Knex);

        jest.spyOn(userService, "loadAllUsers").mockImplementation(() =>
            Promise.resolve([
                {
                    id: 1,
                    username: "rylox",
                    email: "rylox@gmail.com",
                    password: "123456",
                    is_active: true,
                },
            ])
        );

        userController = new UserController(userService);
        req = {} as Request;
        res = ({
            json: jest.fn(),
            redirect: jest.fn(),
            end: jest.fn(),
            status: jest.fn(() => {
                return {
                    json: (data: any) => {
                        console.log(data);
                    },
                };
            }),
        } as any) as Response;
    });

    it("should load a user with his / her email ", async () => {
        await userController.getAllUsers(req, res);
        expect(userService.loadAllUsers).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({
            id: 1,
            username: "rylox",
            email: "rylox@gmail.com",
            password: "123456",
            is_active: true,
        });
        // expect(res.redirect).toBeCalledWith('/?verify=Your account has not been verified')
    });

    // it('should save a new user', async () => {
    //     req.body = {
    //         username: 'rylox',
    //         email: 'rylox@gmail.com',
    //         password: '123456',
    //         passwordRepeat: '123456',
    //         dishList: ["Japanese", "Thai"],
    //     } as any

    //     await userController.register(req, res)
    //     await userService.saveNewUser({
    //         username: 'rylox',
    //         email: 'rylox@gmail.com',
    //         password: '123456',
    //     })
    //     expect(req.body.password).toBe(req.body.passwordRepeat)
    //     expect(userService.loadUserByEmail).toBeCalledWith({ "email": "rylox@gmail.com" })
    //     expect(userService.saveNewUser).toBeCalledWith({ "email": "rylox@gmail.com", "password": "123456", "username": "rylox" })
    //     // expect(res.redirect).toBe('/?verify=Your account is setup successfully. Please login to start.')
    //     // expect(preferenceService.saveUserPreference).toBeCalledWith({ preference: { dishList: ["Japanese", "Thai"], price: "101-200" } })
    //     // expect(recommendationService.saveEachUserRecommendation).toBeCalled()
    // })
});
