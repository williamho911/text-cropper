import Knex from "knex";
import { CardData } from './models';
import tables from "./tables";

export class CardService {
    constructor(private knex: Knex) { }

    saveNewItem = async (photo: string, id: number) => {
        const cardId = await this.knex
            .insert({
                users_id: id,
                photo_url: photo,
                is_finalized: false,
                is_deleted: false,
                created_at: this.knex.fn.now(),
                updated_at: this.knex.fn.now(),
                type: "business_card"
            })
            .into(tables.ITEM)
            .returning('id');
        return cardId[0];
    }

    updateItem = async (id: number) => {
        const newItem = await this.knex(tables.ITEM)
            .where({ id: id })
            .update({
                is_finalized: true,
                updated_at: this.knex.fn.now(),
            })
            .returning("*")
        return newItem[0]
    }

    saveNewBusinessCard = async (cardData: CardData) => {
        const newBusinessCard = await this.knex
            .insert({
                holder_name: cardData.holder_name,
                holder_title: cardData.holder_title,
                email_on_card: cardData.email_on_card,
                company_fax: cardData.fax,
                company_address: cardData.company_address,
                company_website: cardData.company_website,
                item_id: cardData.item_id
            })
            .into("business_card")
            .returning("id")
        return newBusinessCard[0]
    }

    savePhonesOnBusinessCard = async (phone: string, cardID: any) => {
        const newPhoneRow = await this.knex
            .insert({
                phone: phone,
                business_card_id: cardID
            })
            .into(tables.PHONES)
            .returning("*")
        return newPhoneRow
    }

    getAllBusinessCards = async (userID: number | undefined) => {
        const allCards = await this.knex(tables.BUSINESS_CARD)
            .select(
                "item.id",
                "item.photo_url",
                "business_card.id as bCard_id",
                "business_card.*",
                this.knex.raw('ARRAY_AGG(pc.phone) as phones')
            )
            .join("item", "business_card.item_id", "=", "item.id")
            .join("users", "item.users_id", "=", "users.id")
            .join("phones_on_card as pc", "pc.business_card_id", "=", "business_card.id")
            .where({
                "users.id": userID,
                "item.type": "business_card",
            })
            .groupBy("item.id", "business_card.id")
        return allCards
    }

    deleteOneCard = async (cardID: number) => {
        await this.knex(tables.BUSINESS_CARD)
            .where("id", cardID)
            .del()
    }

    deletePhonesOnCard = async (cardID: number) => {
        await this.knex(tables.PHONES)
            .where("business_card_id", cardID)
            .del()
    }
}