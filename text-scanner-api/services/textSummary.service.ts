import Knex from "knex";
import tables from "./tables";

export class TextSummaryService {
    constructor(private knex: Knex) { }

    //create a new item and save the filename of the uploaded image to photo_url of the item
    saveItem = async (photo_url: string | undefined, id: number) => {
        const options = {
            users_id: id,
            type: "document",
            is_finalized: false,
            is_deleted: false,
            created_at: this.knex.fn.now(),
            updated_at: this.knex.fn.now()
        }
        if (photo_url) {
            options["photo_url"] = photo_url
        }
        const itemId = await this.knex
            .insert(options)
            .into(tables.ITEM)
            .returning('id');
        console.log(itemId);

        return itemId[0];
    }

    saveDocumentOCRText = async (item_id: number, OCR_text: string) => {
        const result = await this.knex.select().from('item').where(`id`, `${item_id}`);
        if (!result[0]) { return { success: false } }
        const options = {
            item_id: `${item_id}`,
            OCR_text: `${OCR_text}`,
        }
        const documentId = await this.knex
            .insert(options)
            .into(tables.DOCUMENT)
            .returning('id');
        console.log("documentId", documentId)
        return documentId[0]
    }

    saveSummary = async (item_id: number, document_id: number, summarized_text: string) => {
        console.log("saveSummary trigger")
        const result = await this.knex(tables.DOCUMENT)
            .where("id", `${document_id}`)
            .andWhere("item_id", `${item_id}`)
            .update("summarized_text", `${summarized_text}`)
            .returning('id')
        console.log("result", result)
        const result2 = await this.knex(tables.ITEM)
            .where("id", `${item_id}`)
            .update("is_finalized", true)
            .returning('id')
        console.log("result", result, result2)
        return { success: true }
    }

    getDocuments = async (userId: number) => {
        const result = await this.knex
            .select('document.id', 'document.item_id', 'photo_url', 'OCR_text', 'summarized_text', 'created_at', 'is_finalized')
            .from(tables.ITEM)
            .rightOuterJoin('document', 'item.id', 'item_id')
            .where({
                'item.users_id': userId,
                'item.is_deleted': false,
                'is_finalized': true,
            })
            .orderBy('item.created_at', 'desc')
        console.log(result)
        return result
    }

}