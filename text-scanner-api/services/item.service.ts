import Knex from "knex";
import tables from "./tables";

export class ItemService {
    constructor(private knex: Knex) { }

    checkImageBelongToCurrUser = async (userID: number | undefined, filename: string) => {
        try {
            console.log("Verifying Image Belong To Curr User")
            const item = await this.knex(tables.ITEM)
                .select("*")
                .join("users", "item.users_id", "=", "users.id")
                .where({
                    "users.id": userID,
                    "item.photo_url": filename
                })
            //if the filename of the card belong to the current user, the result is not null
            console.log("item:",item);
            
            if (item) {
                return true
            } else {
                return false
            }
        } catch (error) {
            console.log(error.toString());
            return false;
        }
    }
}