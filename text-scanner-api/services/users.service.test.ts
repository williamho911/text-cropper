import Knex from "knex";
const knexConfig = require("../knexfile");
import { UserService } from "./users.service";
import { IUser } from "./models";
import tables from "./tables";

describe("Login Service Testsuit", () => {
    let knex: Knex;
    let userService: UserService;
    let initUser: IUser = {
        id: 1,
        username: "master",
        email: "master@abc.com",
        password: "123456",
    };

    beforeAll(async () => {
        knex = Knex(knexConfig[process.env.NODE_ENV || "test"]);
        await knex(tables.USERS).del();
        await knex(tables.USERS).insert(initUser);

        userService = new UserService(knex);
    });

    test("load user email", async () => {
        const userOne = await userService.loadUserByEmail({
            email: initUser.email,
        });
        expect(userOne).toBeDefined();
        expect(userOne[0].username).toBe("master");
        console.log(typeof userOne[0].created_at);
    });

    it("should create new user", async () => {
        let initUser2: IUser = {
            username: "tester",
            email: "tester@abc.com",
            password: "123456",
        };
        const newUser = await userService.saveNewUser(initUser2);
        // expect(insertedID).toBe(beforeInsertData[beforeInsertData.length - 1].id + 1)
        expect(newUser[0].username).toBe("tester");
        const afterInsertData = await userService.loadAllUsers();
        console.log("Users: ", afterInsertData[afterInsertData.length - 1]);
        expect(afterInsertData[afterInsertData.length - 1].email).toBe(
            "tester@abc.com"
        );
    });

    afterAll(() => {
        knex.destroy();
    });
});
