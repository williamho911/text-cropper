import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import { Request, Response, NextFunction } from "express";
import jwt from "../jwt";

import "../services/models";
import { logger } from "./logger";
import { UserService } from "../services/users.service";

const permit = new Bearer({ query: "access_token" });

export function createIsLoggedIn(userService: UserService) {
    return async function isLoggedIn(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            const token = permit.check(req);
            //console.log("token", token)
            if (!token || token === "null") {
                return res.status(401).json({ msg: "Permission Denied" });
            }
            const payload = jwtSimple.decode(token, jwt.jwtSecret);
            //console.log("payload", payload)
            const user = await userService.loadUserByID({ id: payload.id });
            if (user) {
                req.user = user;
                return next();
            } else {
                return res.status(401).json({ msg: "Permission Denied" });
            }
        } catch (error) {
            logger.error(error);
            res.status(500).json({ msg: "Internal server error (Guards)" });
        }
    };
}
