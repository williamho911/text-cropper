import winston from "winston";
import { Request, Response, NextFunction } from 'express';

const logFormat = winston.format.printf(function (info) {
    let date = new Date().toISOString();
    return `${date}[${info.level}]: ${info.message}\n`;
});
export const logger = winston.createLogger({
    level: "info",
    format: winston.format.combine(winston.format.colorize(), logFormat),
    transports: [new winston.transports.Console()],
});

export const reqLogger = (req: Request, res: Response, next: NextFunction) => {
    logger.info(`${req.method}: ${req.path}`);
    next();
}
