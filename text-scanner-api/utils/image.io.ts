import aws from "aws-sdk";
import multerS3 from "multer-s3";
import { logger } from "./logger";

const s3 = new aws.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: 'ap-southeast-1'
});

const AWS_S3_IMAGE_BUCKET_NAME = "text-scanner-image"

export const awsStorage = multerS3({
  s3: s3,
  bucket: AWS_S3_IMAGE_BUCKET_NAME,
  metadata: (req, file, cb) => {
    cb(null, { fieldName: file.fieldname });
  },
  key: (req, file, cb) => {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
})

//https://stackoverflow.com/questions/27753411/how-do-i-delete-an-object-on-aws-s3-using-javascript
export async function deleteS3Obj(filename: string) {
  let params = { Bucket: AWS_S3_IMAGE_BUCKET_NAME, Key: filename };
  try {
    await s3.deleteObject(params).promise();
    logger.info("delete s3 object success.")
  } catch (error) {
    logger.info(`delete s3 object error:${error.toString()}`)
  }
}

//https://stackoverflow.com/questions/36942442/how-to-get-response-from-s3-getobject-in-node-js
export async function getS3Obj(filename: string) {
  let params = { Bucket: AWS_S3_IMAGE_BUCKET_NAME, Key: filename };
  try {
    let image = await s3.getObject(params).promise();
    return image;
  } catch (error) {
    logger.info(`get s3 object error:${error.toString()}`)
    return null;
  }
}

//https://docs.aws.amazon.com/AmazonS3/latest/dev/ShareObjectPreSignedURL.html
export function getTimeLimitedImageUrl(filename: string):string {
  try {
    let params = {Bucket: AWS_S3_IMAGE_BUCKET_NAME, Key: filename};
    let url = s3.getSignedUrl('getObject', params);
    return url;
  } catch (error) {
    console.log("get s3 obj url error:", error);
    return filename;
  }
}

