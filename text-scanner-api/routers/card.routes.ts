// import { cardController, isLoggedIn, cardUpload } from '../main';
import { cardController, isLoggedIn, awsUpload } from '../main';

import express from "express"

export const cardRoutes = express.Router()

// cardRoutes.post("/uploadBusinessCard", isLoggedIn, cardUpload.single('image'), cardController.getPythonResult);
cardRoutes.post("/uploadBusinessCard", isLoggedIn, awsUpload.single('image'), cardController.getPythonResult);
cardRoutes.post("/createNewCard", isLoggedIn, cardController.saveNewCardInfo);
cardRoutes.get('/businessCards', isLoggedIn, cardController.loadAllCardsInfo)
cardRoutes.delete('/businessCardID/:index', isLoggedIn, cardController.deleteOneCard)
// cardRoutes.get('/cardImage/:filename', isLoggedIn, cardController.getCardImage);
// cardRoutes.post('/updatecard', isLoggedIn, cardController.updateCard);
