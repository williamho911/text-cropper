import { userController, isLoggedIn, awsUpload } from '../main';
import express from "express";

export const userRoutes = express.Router();

userRoutes.post("/login", userController.login);
userRoutes.post("/login/facebook", userController.loginFacebook);
userRoutes.post("/login/google", userController.loginGoogle);
userRoutes.get("/userInfo", isLoggedIn, userController.getInfo);
userRoutes.post(
    "/register",
    //upload.single("profile_picture_url"),
    awsUpload.single("profile_picture_url"),
    userController.register
);