import { profileController, isLoggedIn, awsUpload } from '../main'

import express from "express"


export const profileRoutes = express.Router()

profileRoutes.get("/profile", isLoggedIn, profileController.getProfile)
profileRoutes.post("/profile/image", isLoggedIn, awsUpload.single('image'), profileController.postProfilePhoto)
profileRoutes.post("/profile/name", isLoggedIn, profileController.postName)
profileRoutes.post("/profile/password", isLoggedIn, profileController.postPassword)