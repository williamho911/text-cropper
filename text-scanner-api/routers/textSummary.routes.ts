import express from "express"
export const textSummaryRoutes = express.Router()
import { awsUpload, textSummaryController } from '../main';

textSummaryRoutes.post("/uploadimg", awsUpload.single('image'), textSummaryController.getOCRTextFromImg);
textSummaryRoutes.post("/generateSummary", textSummaryController.generateSummary);
textSummaryRoutes.post("/saveSummary", textSummaryController.saveSummary)
textSummaryRoutes.get('/textsummaryrecord', textSummaryController.getRecord)
