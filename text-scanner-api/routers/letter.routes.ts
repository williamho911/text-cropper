import { awsUpload, letterController } from "../main";

import express from "express"

export const letterRoutes = express.Router()

letterRoutes.post("/uploadletter", awsUpload.single('image'), letterController.getPythonResult);
letterRoutes.post("/saveletter", letterController.saveLetter)
letterRoutes.get("/letters", letterController.getLetters)
letterRoutes.delete("/deleteletter/:index", letterController.deleteLetter)