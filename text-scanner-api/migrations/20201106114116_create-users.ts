import * as Knex from "knex";

const usersTableName = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(usersTableName, (table) => {
        table.increments();
        table.string("username", 255).notNullable();
        table.string("password", 60).notNullable();
        table.string("email").unique().notNullable();
        table.string("profile_picture_url", 255);
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(usersTableName);
}
