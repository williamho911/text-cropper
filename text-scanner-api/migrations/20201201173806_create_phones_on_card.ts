import * as Knex from "knex";

const phonesTableName = "phones_on_card"

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(phonesTableName, (table) => {
        table.increments();
        table.string("phone", 255)
        table.integer("business_card_id").unsigned();
        table.foreign('business_card_id').references('business_card.id');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(phonesTableName);
}

