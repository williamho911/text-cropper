import * as Knex from "knex";

const usersTableName = "item";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(usersTableName, (table) => {
        table.increments();
        // table.enu('type', ['business_card', 'letter', 'document'])
        table.integer("users_id").unsigned();
        table.foreign('users_id').references('users.id');
        table.string("photo_url", 255)
        table.boolean("is_finalized")
        table.boolean("is_deleted")
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(usersTableName);
}

