import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('document');
    if (hasTable) {
        return knex.schema.alterTable('document', (table) => {
            table.dropColumn('document_id');
            table.integer("item_id").unsigned();
            table.foreign('item_id').references('item.id');
        });
    } else {
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('document');
    if (hasTable) {
        return knex.schema.alterTable('document', (table) => {
            table.dropColumn('item_id');
            table.integer("document_id").unsigned();
            table.foreign('document_id').references('document.id');
        });
    } else {
        return Promise.resolve();
    }
}


