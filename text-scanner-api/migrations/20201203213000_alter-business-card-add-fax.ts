import * as Knex from "knex";

const nameCardTableName = "business_card"

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(nameCardTableName, (table) => {
        table.string("company_fax", 255);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable(nameCardTableName, (table) => {
        table.dropColumn("company_fax");
    })
}

