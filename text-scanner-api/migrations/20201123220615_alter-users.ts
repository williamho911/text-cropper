import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('users');
    if(hasTable){
        return knex.schema.alterTable('users',(table)=>{
            table.string("password", 255).notNullable().alter();
            table.boolean("is_deleted")
        });  
    }else{
        return Promise.resolve();
    }
};

export async function down(knex: Knex) {
    const hasTable = await knex.schema.hasTable("users");
    if(hasTable){
        return knex.schema.alterTable('users',(table)=>{
            table.string("password", 60).notNullable().alter()
            table.dropColumn("is_deleted")
        });
    }else{
        return Promise.resolve();
    }
};