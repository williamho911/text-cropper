import * as Knex from "knex";

const nameCardTableName = "business_card"

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(nameCardTableName, (table) => {
        table.dropColumn('phone_array');
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable(nameCardTableName, (table) => {
        table.specificType('phone_array', 'integer ARRAY');
    })
}

