import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("logo", (table) => {
        table.increments();
        table.string("logo_name", 255);
        table.string("logo_photo_url", 255);
        table.timestamps(false, true);
        table.boolean("is_deleted");
    })
    await knex.schema.createTable("letter", (table) => {
        table.increments();
        table.integer("logo_id").unsigned().notNullable();
        table.foreign("logo_id").references('logo.id')
        table.integer("item_id").unsigned().notNullable();
        table.foreign("item_id").references("item.id")
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("letter");
    await knex.schema.dropTableIfExists("logo");
}

