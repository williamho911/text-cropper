import express from "express";
import { userRoutes } from "./routers/users.routes";
import { cardRoutes } from "./routers/card.routes";
import { textSummaryRoutes} from "./routers/textSummary.routes";
import { isLoggedIn } from "./main";
import { profileRoutes } from "./routers/profile.routes";
import { letterRoutes } from "./routers/letter.routes"
import { itemRoutes } from "./routers/item.routes";

export const routes = express.Router();
routes.use("/", userRoutes);
routes.use("/", isLoggedIn, textSummaryRoutes)
routes.use("/", isLoggedIn ,cardRoutes);
// routes.use("/", isLoggedIn, textSummaryRoutes)
routes.use("/", isLoggedIn, profileRoutes)
routes.use('/', isLoggedIn, letterRoutes)
routes.use('/', isLoggedIn, itemRoutes)
