import * as Knex from "knex";
import { IUser } from "../services/models";
import { hashPassword } from "../utils/hash";

const usersTableName = "users";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(usersTableName).del();

    const userArr: Array<IUser> = [
        {
            username: "demo01",
            password: await hashPassword("123456"),
            email: "demo01@abc.com",
        },
        {
            username: "demo02",
            password: await hashPassword("123456"),
            email: "demo02@abc.com",
        },
    ];
    // Inserts seed entries
    await knex(usersTableName).insert(userArr);
}
