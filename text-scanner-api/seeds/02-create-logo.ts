import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("logo").del();

    // Inserts seed entries
    await knex("logo").insert([
        { 
            id: 1,
            logo_name: "Bank of China",
            logo_photo_url: "bankofchina.jpg",
            is_deleted: false,
        },
        { 
            id: 2,
            logo_name: "CLP Power",
            logo_photo_url: "clppower.jpg",
            is_deleted: false,
        },        
        { 
            id: 3,
            logo_name: "HSBC",
            logo_photo_url: "hsbc.jpg",
            is_deleted: false,
        },        
        { 
            id: 4,
            logo_name: "Inland Revenue Department",
            logo_photo_url: "ird.jpg",
            is_deleted: false,
        },        
        { 
            id: 5,
            logo_name: "Water Supplies Department",
            logo_photo_url: "wsd.jpg",
            is_deleted: false,
        },        
        { 
            id: 6,
            logo_name: "Other",
            logo_photo_url: "other.jpg",
            is_deleted: false,
        }
    ]);
};
