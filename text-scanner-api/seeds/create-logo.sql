INSERT INTO logo (logo_name, logo_photo_url, created_at, updated_at, is_deleted) VALUES ('Bank of China', 'bankofchina.jpg', NOW(), NOW(), 'f');
INSERT INTO logo (logo_name, logo_photo_url, created_at, updated_at, is_deleted) VALUES ('CLP Power', 'clppower.jpg', NOW(), NOW(), 'f');
INSERT INTO logo (logo_name, logo_photo_url, created_at, updated_at, is_deleted) VALUES ('HSBC', 'hsbc.jpg', NOW(), NOW(), 'f');
INSERT INTO logo (logo_name, logo_photo_url, created_at, updated_at, is_deleted) VALUES ('Inland Revenue Department', 'ird.jpg', NOW(), NOW(), 'f');
INSERT INTO logo (logo_name, logo_photo_url, created_at, updated_at, is_deleted) VALUES ('Water Supplies Department', 'wsd.jpg', NOW(), NOW(), 'f');
INSERT INTO logo (logo_name, logo_photo_url, created_at, updated_at, is_deleted) VALUES ('Other', 'other.jpg', NOW(), NOW(), 'f');