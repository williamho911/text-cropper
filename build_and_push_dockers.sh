# build docker images
bash ./python-server/server/build_docker_image.sh
bash ./text-scanner-api/build_docker_image.sh
# tag the images
docker tag text-scanner-express-server:latest tkmok/text-scanner-express-server:latest
docker tag text-scanner-python-server:latest tkmok/text-scanner-python-server:latest
# push them to docker hub
docker push tkmok/text-scanner-express-server:latest
docker push tkmok/text-scanner-python-server:latest