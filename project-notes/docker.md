# Docker build
```bash
cd project dir
sudo docker build -t text-scanner-express-server .
```  
# Push to dockerhub
```bash
#tag the docker
sudo docker tag text-scanner-express-server:latest tkmok/text-scanner-express-server:latest
sudo docker tag text-scanner-python-server:latest tkmok/text-scanner-python-server:latest
# login dockerhub and push
sudo docker login
sudo docker push tkmok/text-scanner-express-server:latest
sudo docker push tkmok/text-scanner-python-server:latest
```  
# Run docker with access to localhost
```bash
# The flag --network host is necessary because it needs to be able to access PostgreSQL database in localhost
sudo docker run -d -it --env-file .env --name=<your-container-name> --network host   <your-image-name>
# without env
sudo docker run -d -it --name=<your-container-name> --network host   <your-image-name>
```  
# To activate python conda env in docker!!!
https://pythonspeed.com/articles/activate-conda-dockerfile/

# Restart stopped docker
First get the stopped CONTAINER ID:  
```bash
docker ps -a
```  
Next,  
```bash
# just start the container
docker start d29ffe4d00ed
# here d29ffe4d00ed is the container id
# or start the container and see the log immediately
docker start d29ffe4d00ed; docker attach d29ffe4d00ed;
```  
# see the console log from the docker
```bash
docker attach d29ffe4d00ed
```  
# list files inside running container
Use docker exec  
https://docs.docker.com/engine/reference/commandline/exec/  
```bash
# container-name can be found using docker ps -a
docker exec -it <container-name> bash
# then we can directly execute bash command to the container
# to list files inside the container
ls
# to quit the bash session of the container
exit
```  
