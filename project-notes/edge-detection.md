# Edge Detection
[4 Point OpenCV getPerspective Transform Example](https://www.pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/)  
[How to Build a Kick-Ass Mobile Document Scanner in Just 5 Minutes](https://www.pyimagesearch.com/2014/09/01/build-kick-ass-mobile-document-scanner-just-5-minutes/)  

&nbsp;

# Python library

&nbsp;

## argparse
[Python 超好用標準函式庫 argparse](https://medium.com/@dboyliao/python-%E8%B6%85%E5%A5%BD%E7%94%A8%E6%A8%99%E6%BA%96%E5%87%BD%E5%BC%8F%E5%BA%AB-argparse-4eab2e9dcc69)  

&nbsp;

## imutils
https://github.com/jrosebr1/imutils  
To be used with OpenCV.  
Can do basic image processing operations such as translation, rotation, resizing, skeletonization on OpenCV image.  
**Common functions:**  

1. Automatic Canny Edge Detection!!!
```python
gray = cv2.cvtColor(logo, cv2.COLOR_BGR2GRAY)
edgeMap = imutils.auto_canny(gray)
cv2.imshow("Original", logo)
cv2.imshow("Automatic Edge Map", edgeMap)
```  
It uses the median of the grayscale pixel intensities to derive the upper and lower thresholds.  

2. 4-point Perspective Transform
perform a 4-point perspective transform of a ROI(Region of interest) in an image and obtain a top-down, "birds eye view" of the ROI.  

3. Sorting Contours
The contours returned from cv2.findContours are unsorted. By using the contours module the the sort_contours function we can sort a list of contours from left-to-right, right-to-left, top-to-bottom, and bottom-to-top, respectively.  

4. grab_contours
```python
cnts = cv2.findContours(...)
cnts = imutils.grab_contours(cnts)
```  
Why do we need grab_contours function? because the OpenCV findContours function output array has the contours result placed in different index in different OpenCV version!!! This function is to solve the compatibility problem!!!
&nbsp;
&nbsp;
# OpenCV

&nbsp;

## findContours
[findContours函数参数详解](https://blog.csdn.net/dcrmg/article/details/51987348)  
[findContours officially api doc](https://docs.opencv.org/3.4/d3/dc0/group__imgproc__shape.html#ga17ed9f5d79ae97bd4c7cf18403e1689a)  
[difference between CV_RETR_LIST,CV_RETR_TREE,CV_RETR_EXTERNAL?](https://stackoverflow.com/questions/8830619/difference-between-cv-retr-list-cv-retr-tree-cv-retr-external)  
