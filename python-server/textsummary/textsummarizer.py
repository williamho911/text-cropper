#found a lib that finish summary in a few lines of words
#pass the textstr into the textsummary function
#We can let the users to set the ratio of summarized text
#We can let the users to get the keywords of the summarized text and used that as a tag for their summary
#Install the lib by $pip install gensim

from gensim.summarization.summarizer import summarize
from gensim.summarization import keywords

def textsummary (text, ratio=0.5, keyWordNum=5):
  return [summarize(text, ratio),keywords(text, words=keyWordNum)]

[summarizedText,generatedKeywords] = textsummary(str, ratio=0.5)

print(summarizedText)
print(generatedKeywords)