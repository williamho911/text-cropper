import numpy as np
import cv2
import imutils
from skimage.filters import threshold_local


class EdgeDetector:

    def __init__(self):
        pass

    @staticmethod
    def order_points(pts):
        # initialzie a list of coordinates that will be ordered
        # such that the first entry in the list is the top-left,
        # the second entry is the top-right, the third is the
        # bottom-right, and the fourth is the bottom-left
        rect = np.zeros((4, 2), dtype="float32")
        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis=1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]
        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis=1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
        # return the ordered coordinates
        return rect

    @staticmethod
    def four_point_transform(image, pts):
        # obtain a consistent order of the points and unpack them
        # individually
        rect = EdgeDetector.order_points(pts)
        (tl, tr, br, bl) = rect
        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))
        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))
        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype="float32")
        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
        # return the warped image
        return warped, [M, maxWidth, maxHeight]

    @staticmethod
    def get_canny_edged_img(image):
        # convert the image to grayscale, blur it, and find edges
        gray = cv2.cvtColor(image.copy(), cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(gray, 75, 200)
        return edged

    @staticmethod
    def find_contours(edged_img):
        # find the contours in the edged image, keeping only the
        # largest ones, and initialize the screen contour
        cnts = cv2.findContours(
            edged_img.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:5]
        # loop over the contours
        for c in cnts:
            # approximate the contour
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * peri, True)
            # if our approximated contour has four points, then we
            # can assume that we have found our screen
            if len(approx) == 4:
                return approx

    @staticmethod
    def get_perspective_img(image):
        # apply the four point transform to obtain a top-down
        # view of the original image

        original_image = image.copy()

        ratio = image.shape[0] / 500.0
        image = imutils.resize(image, height=500)

        edged = EdgeDetector.get_canny_edged_img(image)
        cnt = EdgeDetector.find_contours(edged)
        # contour points are not 4, can't apply perspective transform
        if cnt is None:
            return None
        pts = cnt.reshape(4, 2) * ratio
        warped = EdgeDetector.four_point_transform(original_image, pts)[0]
        return warped

    @staticmethod
    def get_grayscale_img(image):
        # convert the warped image to grayscale, then threshold it
        # to give it that 'black and white' paper effect
        image = image.copy()
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        T = threshold_local(image, 11, offset=10, method="gaussian")
        image = (image > T).astype("uint8") * 255
        return image


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("image_path", help="image path for edge detection")
    args = parser.parse_args()

    image = cv2.imread(args.image_path)
    ratio = image.shape[0] / 500.0
    orig = image.copy()
    image = imutils.resize(image, height=500)

    edged = EdgeDetector.get_canny_edged_img(image)

    cv2.imshow("Image", image)
    cv2.imshow("Edged", edged)
    # cv2.imwrite(temp folder)

    screenCnt = EdgeDetector.find_contours(edged)
    if screenCnt is not None:
        print("contours: ", screenCnt)
        cv2.drawContours(image, [screenCnt], -1, (0, 255, 0), 2)
        cv2.imshow("Outline", image)

        transform_output = EdgeDetector.four_point_transform(
            orig, screenCnt.reshape(4, 2) * ratio)
        print("transform_output: ", transform_output)
        warped = transform_output[0]
        cv2.imshow("perspective view", imutils.resize(warped, height=650))

        # test get_perspective_img function
        perspective_img = EdgeDetector.get_perspective_img(orig)
        cv2.imshow("get_perspective_img", imutils.resize(perspective_img, height=650))

        warped = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
        T = threshold_local(warped, 11, offset=10, method="gaussian")
        warped = (warped > T).astype("uint8") * 255
        # show the original and scanned images
        print("STEP 3: Apply perspective transform")
        cv2.imshow("Original", imutils.resize(orig, height=650))
        cv2.imshow("Scanned", imutils.resize(warped, height=650))
    else:
        print("contour points are not 4")

    cv2.waitKey(0)
    cv2.destroyAllWindows()
