import re
from names_dataset import NameDataset

class CardTextParser:
    def __init__(self, job_title_file_path):
        self._job_titles_list = self._load_job_titles(job_title_file_path)

    def _load_job_titles(self, job_title_file_path):
        job_titles_list = []
        with open(job_title_file_path, "r") as f:
            for each in f:
                job_titles_list.append(each.lower())
        return job_titles_list

    # For refactoring reference~
    # https://stackoverflow.com/questions/36196060/get-group-match-in-re-sub-in-python
    # results = []

    # def capture_and_kill(match):
    #     results.append(match)
    #     return ""

    # string = "abcdef123"
    # string = re.sub("(\d+)", capture_and_kill, string)
    # results[0].group(1) # => '123'
    #
    #
    #
    # def _find_and_replace(text, pattern):
    #     pass

    def extract_emails(self, text, email_pattern=r"[a-z0-9\.\-+_]+[ ]*@[ ]*[a-z0-9\.\-+_ ]+\.[a-z]+"):
        emails = re.findall(email_pattern, text.replace(" ", "\n"))
        final_emails = []
        for email in emails:
            text = text.replace(email, "")
            final_emails.append(email.replace(" ", ""))
        return text,final_emails

    def extract_websites(self, text, website_pattern=r"[a-z0-9\.\-+_]+\.[a-z]+"):
        websites = re.findall(website_pattern, text)
        return text,websites

    # fax = re.search(r"((F|f)ax:? ?|(F|f):? ?)(((\*1)[ -.])?\(?(\d{3})?\)?[ -.]?(\d*)?[ -.]?(\d*)?[ -.]?(\d*))", text, re.MULTILINE)
    # print("fax: ", fax)

    def extract_faxs(self, text, fax_pattern=r"((F|f)ax:? ?|(F|f):? ?)(((\*1)[ -.])?\(?(\d{3})?\)?[ -.]?(\d*)?[ -.]?(\d*)?[ -.]?(\d*))", refine_fax_pattern=r"[a-zA-Z:]*"):
        matches = re.finditer(fax_pattern, text, re.MULTILINE)
        fax = ["fax"]
        for match in matches:
            i = 0
            if len(match.group()) > len(fax[i]):
                fax.pop(i)
                fax.append(match.group())
                i = 1 + 1
        text = text.replace(fax[0], "")
        fax = re.sub(refine_fax_pattern, "", fax[0]).strip()
        return text,fax

    # tels = [x.group(0) for x in re.finditer(r"\(? ?(\d[ -.]+)? ?\)?\(?(\d+)\)?[ -.]?(\d+)?[ -.]?(\d+)", text, re.MULTILINE)]

    def extract_tels(self, text, tel_pattern=r"(\(*[ .-]*\d{1,3}[ .-]*\)*)(\(*[ .-]*\d{2,4}[ .-]*\)*)(\(*[ .-]*\d{2,4}[ .-]*\)*)(\(*[ .-]*\d{2,4}[ .-]*\)*)"):
        tels = [x.group(0) for x in re.finditer(tel_pattern, text, re.MULTILINE)]
        telephones = []
        for tel in tels:
            if len(tel.replace(",", "").replace("-", "")) > 7:
                telephones.append(tel)
        return text,telephones

    def extract_job_title(self, text, job_titles_list=None):
        # if no job titles list provided, use the default one
        if job_titles_list is None:
            job_titles_list = self._job_titles_list

        # get the job titles appear in the text
        job_titles_in_text = [x for x in job_titles_list if x in text.replace("  ", "\n").lower()]

        job_title = ""
        if job_titles_in_text:
            job_title = job_titles_in_text[0].replace('\n', '')
            if len(job_titles_in_text) > 1:
                for title in job_titles_in_text:
                    if len(title) > len(job_title):
                        job_title = title.replace('\n', '')
            else:
                job_title = job_titles_in_text[0]

        job_title = job_title.title()
        # delete the job_title string in the text
        text = text.replace(job_title, "")
        return text, job_title


    def extract_address(self, text, job_titles_list=None):
        text = text.replace(',\n', ', ')  # Join address with ','
        lines = text.strip().split('\n')
        address = ""
        for line in lines:
            if len(line.split(',')) > 2:
                # print(line)
                address = line
                text = text.replace(address, "")
        return text, address

    def _most_frequent(self, List): 
        counter = 0
        num = List[0] 
      
        for i in List: 
            curr_frequency = List.count(i) 
            if(curr_frequency> counter): 
                counter = curr_frequency 
                num = i 

        return num 

    def extract_names(self, text, job_titles_list=None):
        name_matcher = NameDataset()
        split_text = text.split()
        name = []
        for word in split_text:
            if name_matcher.search_first_name(word):
                name.append(word)
            elif name_matcher.search_last_name(word):
                name.append(word)
        if len(name) > 1:
            name = list(dict.fromkeys(name))
        rows = text.split("\n")
        final_name = []
        # if name: 
        for eachName in name:
          eachRow = [row for row in rows if eachName in row]
          final_name.append(eachRow[0])

        if len(final_name) > 1:
          final_name = self._most_frequent(final_name)
        return text, final_name

# card_data = {
#     "photo": pic_path,
#     "name": name,
#     "job_title": job_title,
#     "tel": telephone,
#     "fax": fax,
#     "website": websites,
#     "email": final_emails,
#     "address": address
# }
