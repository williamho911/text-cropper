import re
import pytesseract
import imutils
from app.utils.logger import logger


def get_eng_text(image):
    try:
        text = pytesseract.image_to_string(image, lang="eng").strip()
        text_data = pytesseract.image_to_data(image, lang="eng")
        return text, text_data
    except Exception as e:
        logger.error(f"{e}")
        return "", ""


def get_chi_text(image):
    try:
        return pytesseract.image_to_string(image)
    except Exception as e:
        logger.error(f"{e}")
        return ""


def get_multi_lan_text(image):
    try:
        return pytesseract.image_to_string(image)
    except Exception as e:
        logger.error(f"{e}")
        return ""


def strip_non_word_text(text, text_data):
    for line_num, data in enumerate(text_data.splitlines()):
        if line_num != 0:
            data = data.split()
            if len(data) > 11 and int(data[10]) < 55:
                regex = r'(' + str(re.escape(data[11])) + r')(?=\s)'
                text = re.sub(regex, "", text)
    return text


def get_text_orientation(image, center=None, scale=1.0):
    try:
        osd = pytesseract.image_to_osd(image)
        angle = int(re.search(r'(?<=Rotate: )\d+', osd).group(0))
        return angle
    except Exception as e:
        logger.error(f"{e}")
        return 0


def rotate_img_to_text_orientation(image):
    try:
        osd = pytesseract.image_to_osd(image)
        angle = int(re.search(r'(?<=Rotate: )\d+', osd).group(0))
        if angle == 270:
            angle = -90
        logger.debug(f"text orientation angle: {angle}")
        return imutils.rotate_bound(image, angle)
    except Exception as e:
        logger.error(f"{e}")
        return image
