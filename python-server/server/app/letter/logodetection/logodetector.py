import tensorflow as tf
from app.utils.logger import logger
import time
import numpy as np
from app.config import appconfigs

class LogoDetector:
    def __init__(self):
        # load the model to memory
        logger.info("loading logo object detection model")
        start_time = time.time()
        self._detect_fn = tf.saved_model.load(appconfigs.LOGO_SAVED_MODEL_PATH)
        end_time = time.time()
        elapsed_time = end_time - start_time
        logger.info(f"finished loading logo model with {elapsed_time} seconds")

        # mapping for classification number to logo name
        self._logo_tag_dictionary = {1: "boc", 2: "clp", 3: "hsbc", 4: "ird", 5: "wsd"}

    def get_logo_tag(self, image_np):
        # Get logo name

        # Args:
        #     image (numpy.ndarray): the image with cards inside

        # Returns:
        #     logo_tag (str): logo tag

        # https://stackoverflow.com/questions/43272848/what-is-dimension-order-of-numpy-shape-for-image-data
        height, width, *_ = image_np.shape
        logger.debug(f"image size (height,width): {height},{width}")

        # The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
        input_tensor = tf.convert_to_tensor(image_np)
        # The model expects a batch of images, so add an axis with `tf.newaxis`.
        input_tensor = input_tensor[tf.newaxis, ...]

        # RUN MODEL
        detections = self._detect_fn(input_tensor)

        # All outputs are batches tensors.
        # Convert to numpy arrays, and take index [0] to remove the batch dimension.
        # We're only interested in the first num_detections.
        num_detections = int(detections.pop('num_detections'))
        detections = {key: value[0, :num_detections].numpy()
                      for key, value in detections.items()}
        detections['num_detections'] = num_detections

        # detection_classes should be ints.
        detections['detection_classes'] = detections['detection_classes'].astype(np.int64)

        # Interpret and return the result:
        # only return the highest similarity
        detection_scores = detections["detection_scores"][0]
        detection_class = detections["detection_classes"][0]

        logo_tag = None
        # if score <= 0.5, no logo found
        if detection_scores <= 0.5:
            logo_tag = None
        # score > 0.5, logo found
        else:
            print(detection_class)
            logo_tag = self._logo_tag_dictionary[detection_class]

        return logo_tag
