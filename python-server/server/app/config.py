import tensorflow as tf
from os import path, environ
import app
import warnings
import logging

from dotenv import load_dotenv
from pathlib import Path

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Suppress TensorFlow logging (1)
tf.get_logger().setLevel('ERROR')           # Suppress TensorFlow logging (2)


# Enable GPU dynamic memory allocation
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

warnings.filterwarnings('ignore')   # Suppress Matplotlib warnings

logging.getLogger("PIL.TiffImagePlugin").propagate = False  # Suppress PIL logging

# Suppress aws boto3 logging
logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logging.getLogger('s3transfer').setLevel(logging.CRITICAL)
logging.getLogger('urllib3').setLevel(logging.CRITICAL)

class Configs:
    _APP__PACKAGE_ABSOLUTE_PATH: str = app.__path__[0]
    _CARD_MODEL_DIRECTORY: str = path.join(
        _APP__PACKAGE_ABSOLUTE_PATH, "card", "cardobjectdetection")
    _LOGO_MODEL_DIRECTORY: str = path.join(
        _APP__PACKAGE_ABSOLUTE_PATH, "letter", "logodetection")
    CARD_SAVED_MODEL_PATH: str = path.join(_CARD_MODEL_DIRECTORY, "saved_model")
    LOGO_SAVED_MODEL_PATH: str = path.join(_LOGO_MODEL_DIRECTORY, "saved_model")

    LOGGING_CONFIG_PATH: str = path.join(
        _APP__PACKAGE_ABSOLUTE_PATH, "utils", 'logging.conf')

    # {app_folder}/../../../image_storage
    IMG_STORAGE_PATH: str = path.join(_APP__PACKAGE_ABSOLUTE_PATH, "..", "..", "..", "image_storage")

    JOB_TITLES_DICT_PATH: str = path.join(_APP__PACKAGE_ABSOLUTE_PATH,"card","cardtextparser","job.txt")


appconfigs = Configs()

if __name__ == "__main__":
    print()
