from fastapi import APIRouter

from app.api.endpoints import card
from app.api.endpoints import document
from app.api.endpoints import letter

api_router = APIRouter()
api_router.include_router(card.router, prefix="/card", tags=["card"])
api_router.include_router(letter.router, prefix="/letter", tags=["letter"])
api_router.include_router(document.router, prefix="/document", tags=["document"])