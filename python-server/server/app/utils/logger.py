import logging
import logging.config
from app.config import appconfigs

# setup loggers
from os import path
logging.config.fileConfig(appconfigs.LOGGING_CONFIG_PATH, disable_existing_loggers=False)
# logging.config.fileConfig(appconfigs.LOGGING_CONFIG_PATH)

# get logger with name dev
logger = logging.getLogger('dev')
