from PIL import Image
from numpy import array
from app.utils.logger import logger
from app.config import appconfigs
import os
import cv2

import boto3
from io import BytesIO

# https://gist.github.com/ghandic/a48f450f3c011f44d42eea16a0c7014d
class S3ImagesInvalidExtension(Exception):
    pass

class S3ImagesUploadFailed(Exception):
    pass

class S3Images(object):
    
    """Useage:
    
        images = S3Images(aws_access_key_id='fjrn4uun-my-access-key-589gnmrn90', 
                          aws_secret_access_key='4f4nvu5tvnd-my-secret-access-key-rjfjnubu34un4tu4', 
                          region_name='eu-west-1')
        im = images.from_s3('my-example-bucket-9933668', 'pythonlogo.png')
        im
        images.to_s3(im, 'my-example-bucket-9933668', 'pythonlogo2.png')
    """
    
    def __init__(self, aws_access_key_id, aws_secret_access_key, region_name, bucket_name):
        self.s3 = boto3.client('s3', aws_access_key_id=aws_access_key_id, 
                                     aws_secret_access_key=aws_secret_access_key, 
                                     region_name=region_name)
        self._bucket_name = bucket_name
        

    def from_s3(self, key):
        file_byte_string = self.s3.get_object(Bucket=self._bucket_name, Key=key)['Body'].read()
        return Image.open(BytesIO(file_byte_string))
    

    def to_s3(self, img, key):
        buffer = BytesIO()
        img.save(buffer, self.__get_safe_ext(key))
        buffer.seek(0)
        sent_data = self.s3.put_object(Bucket=self._bucket_name, Key=key, Body=buffer)
        if sent_data['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise S3ImagesUploadFailed('Failed to upload image {} to bucket {}'.format(key, self._bucket_name))
        
    def __get_safe_ext(self, key):
        ext = os.path.splitext(key)[-1].strip('.').upper()
        if ext in ['JPG', 'JPEG']:
            return 'JPEG' 
        elif ext in ['PNG']:
            return 'PNG' 
        else:
            raise S3ImagesInvalidExtension('Extension is invalid') 

_AWS_ACCESS_KEY_ID = os.getenv("AWS_ACCESS_KEY_ID")
_AWS_SECRET_ACCESS_KEY = os.getenv("AWS_SECRET_ACCESS_KEY")
_AWS_REGION_NAME = os.getenv("AWS_REGION_NAME")
_AWS_S3_IMAGE_BUCKET_NAME = os.getenv("AWS_S3_IMAGE_BUCKET_NAME")

# print("AWS_ACCESS_KEY_ID", _AWS_ACCESS_KEY_ID)
# print("AWS_SECRET_ACCESS_KEY", _AWS_SECRET_ACCESS_KEY)
# print("AWS_REGION_NAME", _AWS_REGION_NAME)

s3images = S3Images(_AWS_ACCESS_KEY_ID, _AWS_SECRET_ACCESS_KEY, _AWS_REGION_NAME, _AWS_S3_IMAGE_BUCKET_NAME)
def load_image_into_numpy_array(image_filename):
    logger.info(f"load image with image path: {image_filename}")
    image =  s3images.from_s3(image_filename)
    width, height = image.size
    logger.debug(f"Image.open size (width,height): {width},{height}")
    image_np = array(image)
    logger.debug(f"image_np shape: {image_np.shape}")
    return image_np

def save_numpy_image(img_filename,image_np):
    # Numpy array to PIL image format
    PIL_image = Image.fromarray(image_np)
    s3images.to_s3(PIL_image, img_filename)
    logger.info(f"uploaded card image to s3 with name: {img_filename}")

def image_to_numpy_array(image):
    # Load an image from file into a numpy array.

    # Puts image into numpy array to feed into tensorflow graph.
    # Note that by convention we put it into a numpy array with shape
    # (height, width, channels), where channels=3 for RGB.

    # Args:
    #   image: the image opened with PIL.Image.open(img_path)

    # Returns:
    #   uint8 numpy array with shape (img_height, img_width, 3)

    return array(image)


# def load_image_into_numpy_array(image_path):
#     # Load an image from file into a numpy array.

#     # Note that by convention we put it into a numpy array with shape
#     # (height, width, channels), where channels=3 for RGB.

#     # Args:
#     #   image_path: the image path

#     # Returns:
#     #   uint8 numpy array with shape (img_height, img_width, 3)
#     logger.info(f"load image with image path: {image_path}")
#     image = Image.open(image_path)
#     width, height = image.size
#     logger.debug(f"Image.open size (width,height): {width},{height}")
#     image_np = array(image)
#     logger.debug(f"image_np shape: {image_np.shape}")
#     return image_np

# save numpy image to local folder
# def save_numpy_image(img_name,image_np):
#     card_image_path = os.path.join(appconfigs.IMG_STORAGE_PATH, img_name)
#     logger.info(f"save card image with path: {card_image_path}")
#     cv2.imwrite(card_image_path, cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR))

# def save_numpy_image(img_name,image_np):
#     card_image_path = os.path.join(appconfigs.IMG_STORAGE_PATH, img_name)
#     logger.info(f"save card image with path: {card_image_path}")
#     cv2.imwrite(card_image_path, cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR))