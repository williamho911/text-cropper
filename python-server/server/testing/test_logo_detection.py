import sys
sys.path.append("..")
from app.utils.image_io import load_image_into_numpy_array
from app.letter.logodetection.logodetector import LogoDetector

original_image_path = "/home/tkmok/Downloads/hsbc.jpg"
img_np = load_image_into_numpy_array(original_image_path)

logoDetector = LogoDetector()
logo_tag = logoDetector.get_logo_tag(img_np)

# {1: "boc", 2: "clp", 3: "hsbc", 4: "ird", 5: "wsd"}
print("logo_tag:",logo_tag)
