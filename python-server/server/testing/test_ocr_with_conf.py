import sys
sys.path.append("..")
from os.path import isfile, join
from os import listdir
from argparse import ArgumentParser
from app.utils.image_io import load_image_into_numpy_array
from app.document.ocr import ocr
from app.utils.logger import logger
import cv2
import pytesseract
import re

path = "/home/tkmok/Workspace/gitlab/cohort11-prj3/image_storage"
only_img_files =[join(path, "2.jpg")]
image_path=only_img_files[0]

image_np = load_image_into_numpy_array(image_path)
rotated_orig=image_np
rotated_orig = ocr.rotate_img_to_text_orientation(image_np)

text = pytesseract.image_to_string(rotated_orig, lang="eng").strip()
print("text: ",text)
text_data = pytesseract.image_to_data(rotated_orig, lang="eng")
print(f"text_data: {text_data}")
for line_num, data in enumerate(text_data.splitlines()):
    if line_num!=0:
        data = data.split()
        if len(data) > 11 and int(data[10]) < 55:
            regex = r'\b( {0}' + str(re.escape(data[11])) + r' {0,1})\b'
            text = re.sub(regex, "", text)

print("text after refine: ",text)