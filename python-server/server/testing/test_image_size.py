from PIL import Image
import cv2
import numpy as np
import argparse
import imutils

# path="/home/tkmok/Pictures/test_photo/11.jpg"
path="/home/tkmok/Workspace/gitlab/cohort11-prj3/image_storage/image-1606422233394.jpeg"

print("image path:",path)
image=Image.open(path)
width, height = image.size
print("image width:",width,"height:",height)
image_np=np.array(image)
print("image_np shape:",image_np.shape)