# You must pass the application as an import string to enable 'reload' or 'workers'.
# https://github.com/tiangolo/fastapi/issues/1495#issuecomment-671872432
# uvicorn.run(app,host=...) -> uvicorn.run("debug_server:app",host=...)
import uvicorn
from app.app import app
from app.config import appconfigs
if __name__ == "__main__":
    uvicorn.run("debug_server:app", host="127.0.0.1",
                port=5000, debug=True, reload=True)
