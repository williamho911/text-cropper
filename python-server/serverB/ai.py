import os
import sys
import requests
import time
# from object_detection.utils import label_map_util
# from object_detection.utils import visualization_utils as viz_utils
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import warnings
import cv2
from skimage.filters import threshold_local
import argparse
import imutils
import re
import pytesseract
from PIL import Image
from numpy import asarray
from names_dataset import NameDataset
# from find_job_titles import FinderAcora
import json
import pathlib
import tensorflow as tf
# import nltk
# import spacy
# from spacy import displacy
from collections import Counter
# import en_core_web_sm
# nlp = en_core_web_sm.load()
# from requests_html import HTML
m = NameDataset()
import uuid

def order_points(pts):
	# initialzie a list of coordinates that will be ordered
	# such that the first entry in the list is the top-left,
	# the second entry is the top-right, the third is the
	# bottom-right, and the fourth is the bottom-left
	rect = np.zeros((4, 2), dtype = "float32")
	# the top-left point will have the smallest sum, whereas
	# the bottom-right point will have the largest sum
	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]
	# now, compute the difference between the points, the
	# top-right point will have the smallest difference,
	# whereas the bottom-left will have the largest difference
	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]
	# return the ordered coordinates
	return rect

def four_point_transform(image, pts):

	rect = order_points(pts)
	(tl, tr, br, bl) = rect

	widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
	widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
	maxWidth = max(int(widthA), int(widthB))

	heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
	heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
	maxHeight = max(int(heightA), int(heightB))

	dst = np.array([
		[0, 0],
		[maxWidth - 1, 0],
		[maxWidth - 1, maxHeight - 1],
		[0, maxHeight - 1]], dtype = "float32")
	# compute the perspective transform matrix and then apply it
	M = cv2.getPerspectiveTransform(rect, dst)
	warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
	# return the warped image
	return warped, [M, maxWidth, maxHeight]

def rotate(image, center = None, scale = 1.0):
    angle=int(re.search('(?<=Rotate: )\d+', pytesseract.image_to_osd(image)).group(0))
    return angle

warnings.filterwarnings('ignore')   # Suppress Matplotlib warnings

def load_image_into_numpy_array(path):
    """Load an image from file into a numpy array.

    Puts image into numpy array to feed into tensorflow graph.
    Note that by convention we put it into a numpy array with shape
    (height, width, channels), where channels=3 for RGB.

    Args:
      path: the file path to the image

    Returns:
      uint8 numpy array with shape (img_height, img_width, 3)
    """
    return np.array(Image.open(path))

def most_frequent(List): 
    counter = 0
    num = List[0] 
      
    for i in List: 
        curr_frequency = List.count(i) 
        if(curr_frequency> counter): 
            counter = curr_frequency 
            num = i 
  
    return num 

def run_model(model, job_titles, image_path, save_folder_path):
    print(image_path)
    print('Running inference for {}... '.format(image_path), end='')
    image = Image.open(image_path)
    image_np = load_image_into_numpy_array(image_path)
    outputs = []
    # cv2.imwrite(os.path.join(save_folder_path, 'after' + '.jpg'),cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR))

    # Things to try:
    # Flip horizontally
    # image_np = np.fliplr(image_np).copy()

    # Convert image to grayscale
    # image_np = np.tile(
    #     np.mean(image_np, 2, keepdims=True), (1, 1, 3)).astype(np.uint8)

    # The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
    input_tensor = tf.convert_to_tensor(image_np)
    # The model expects a batch of images, so add an axis with `tf.newaxis`.
    input_tensor = input_tensor[tf.newaxis, ...]

    # input_tensor = np.expand_dims(image_np, 0)
    detections = model(input_tensor)

    # All outputs are batches tensors.
    # Convert to numpy arrays, and take index [0] to remove the batch dimension.
    # We're only interested in the first num_detections.
    num_detections = int(detections.pop('num_detections'))
    detections = {key: value[0, :num_detections].numpy()
                  for key, value in detections.items()}
    detections['num_detections'] = num_detections

    # detection_classes should be ints.
    detections['detection_classes'] = detections['detection_classes'].astype(np.int64)

    width, height = image.size
    image_np_temp = image_np.copy()
    print("width: ", width,"height: ", height)
    # print(detections['detection_scores'][0][0])
    for i in range(100):
      #if type == 1 (card) and socres > 50%
      if detections['detection_classes'][i]==1 and detections['detection_scores'][i] > 0.5 :
        box = detections['detection_boxes'][i]
        x_min_coord = int((box[0] * height) + -150)
        y_min_coord = int((box[1] * width) + -150)
        x_max_coord = int((box[2] * height) + 150)
        y_max_coord = int((box[3] * width) + 150)
        print((image_np_temp[x_min_coord:x_max_coord, y_min_coord:y_max_coord, :]).shape)
        card_image = image_np_temp[x_min_coord:x_max_coord, y_min_coord:y_max_coord, :]
        
        cv2.imwrite(os.path.join(save_folder_path, 'card'+str(i) + '.jpg'),cv2.cvtColor(card_image, cv2.COLOR_RGB2BGR))
        # data = np.zeros((height, width, 3), dtype=np.uint8)
        # data[0:256, 0:256] = [255, 0, 0] # red patch in upper left
        # img = Image.fromarray(data, 'RGB')


        image = card_image #your image here
        # cv2.imwrite(os.path.join(save_folder_path, 'after2' + '.jpg'),cv2.cvtColor(image, cv2.COLOR_RGB2BGR))
        ratio = image.shape[0] / 500.0
        # print(ratio)
        # cv2.imwrite(os.path.join(save_folder_path, 'after2' + '.jpg'),cv2.cvtColor(image, cv2.COLOR_RGB2BGR))
        orig = image.copy()

        image = imutils.resize(image, height = 500)
        # convert the image to grayscale, blur it, and find edges
        # in the image
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(gray, 75, 200)
        # show the original image and the edge detected image
        print("STEP 1: Edge Detection")

        cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        print("STEP 1.1")
        cnts = imutils.grab_contours(cnts)
        print("STEP 1.2")
        cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:5]
        print("STEP 1.3")
        # loop over the contours
        for c in cnts:
          # approximate the contour
          print("STEP 1.3.1")
          peri = cv2.arcLength(c, True)
          print("STEP 1.3.2")
          approx = cv2.approxPolyDP(c, 0.02 * peri, True)
          # if our approximated contour has four points, then we
          # can assume that we have found our screen
          print("STEP 1.3.3")
          if len(approx) == 4:
              screenCnt = approx
              break
        # show the contour (outline) of the piece of paper
        print("STEP 2: Find contours of paper")
        cv2.drawContours(image, [screenCnt], -1, (0, 255, 0), 2)
        #cv2.imshow("Outline", image)
        # cv2_imshow(image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        
        # apply the four point transform to obtain a top-down
        # view of the original image
        warped = four_point_transform(orig, screenCnt.reshape(4, 2) * ratio)[0]
        # convert the warped image to grayscale, then threshold it
        # to give it that 'black and white' paper effect
        warped = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
        T = threshold_local(warped, 11, offset = 10, method = "gaussian")
        warped = (warped > T).astype("uint8") * 255
        # show the original and scanned images
        print("STEP 3: Apply perspective transform")
        # cv2_imshow(imutils.resize(orig, height = 650))
        # cv2_imshow(imutils.resize(warped, height = 650))
        # cv2.waitKey(0)

        key_for_transformation = four_point_transform(orig, screenCnt.reshape(4, 2) * ratio)[1]
        adjusted = cv2.warpPerspective(orig, key_for_transformation[0], (key_for_transformation[1], key_for_transformation[2]))
        rotation_of_orig = rotate(adjusted)
        rotated_orig = imutils.rotate_bound(adjusted,rotation_of_orig)

        rotated = imutils.rotate_bound(warped,rotation_of_orig)
        rotated_array = asarray(rotated)

        img_name: str = 'rotated_card_%s.jpg' % str(uuid.uuid4())
        pic_path =os.path.join(save_folder_path, img_name)
        cv2.imwrite(pic_path,cv2.cvtColor(rotated_orig, cv2.COLOR_RGB2BGR))

        # pic_path = os.path.join('rotate_card3'+str(i) + '.jpg')
        # cv2.imwrite(os.path.join('export', 'card3'+str(i) + '.jpg'),cv2.cvtColor(imutils.resize(warped, height = 650), cv2.COLOR_RGB2BGR))
        # cv2.imwrite(os.path.join('export', 'orig_card3'+str(i) + '.jpg'),cv2.cvtColor(rotated_array, cv2.COLOR_RGB2BGR))
        # cv2.imwrite(os.path.join('export', 'rotate_card3'+str(i) + '.jpg'),cv2.cvtColor(rotated_orig, cv2.COLOR_RGB2BGR))

        text = pytesseract.image_to_string(rotated_orig, lang="eng").strip()
        print(text)
        text_data = pytesseract.image_to_data(rotated_orig, lang="eng")
        print(text_data)
        for x, b in enumerate(text_data.splitlines()):
          if x!=0:
            b = b.split()
            if len(b) > 11:
              if int(b[10]) < 55:
                # regex = r'\b(\s{1}' + str(re.escape(b[11])) + r' {0,1})\b'
                regex = r'(' + str(re.escape(b[11])) + r')(?=\s)'
                print(regex)
                text = re.sub(regex, "", text)

        print(text)

        emails = re.findall(r"[a-z0-9\.\-+_]+[ ]*@[ ]*[a-z0-9\.\-+_ ]+\.[a-z]+", text.replace(" ", "\n"))
        final_emails = []
        for email in emails:
          text = text.replace(email, "")
          final_emails.append(email.replace(" ", ""))
        print("email: ", final_emails)

        websites = re.findall(r"[a-z0-9\.\-+_]+\.[a-z]+", text)
        print("websites: ", websites)

        # fax = re.search(r"((F|f)ax:? ?|(F|f):? ?)(((\*1)[ -.])?\(?(\d{3})?\)?[ -.]?(\d*)?[ -.]?(\d*)?[ -.]?(\d*))", text, re.MULTILINE)
        # print("fax: ", fax)

        matches = re.finditer(r"((F|f)ax:? ?|(F|f):? ?)(((\*1)[ -.])?\(?(\d{3})?\)?[ -.]?(\d*)?[ -.]?(\d*)?[ -.]?(\d*))", text, re.MULTILINE)
        fax = ["fax"]
        for matchNum, match in enumerate(matches, start=1):
          i = 0
          if len(match.group()) > len(fax[i]):
            fax.pop(i)
            fax.append(match.group())
            i = 1 + 1 
        text = text.replace(fax[0], "")
        fax = re.sub(r"[a-zA-Z:]*", "", fax[0]).strip()
        print(fax)

        # tels = [x.group(0) for x in re.finditer(r"\(? ?(\d[ -.]+)? ?\)?\(?(\d+)\)?[ -.]?(\d+)?[ -.]?(\d+)", text, re.MULTILINE)]
        tels = [x.group(0) for x in re.finditer(r"(\(*[ .-]*\d{1,3}[ .-]*\)*)(\(*[ .-]*\d{2,4}[ .-]*\)*)(\(*[ .-]*\d{2,4}[ .-]*\)*)(\(*[ .-]*\d{2,4}[ .-]*\)*)", text, re.MULTILINE)]
        telephone = []
        # tels = re.findall(r"\(?(\d[ -.]+)?\)?\(?(\d+)\)?[ -.]?(\d+)?[ -.]?(\d+)", text, re.MULTILINE)
        for tel in tels:
          # print(tel)
          if len(tel.replace(",", "").replace("-", "")) > 7:
            telephone.append(tel)
        # print(tels)
        print(telephone)
        # print(text.strip())

#-------------------------------------------------------------------------
        allJobTitles = [x for x in job_titles if x in text.replace("  ", "\n").lower()]
        job_title = ""
        if allJobTitles:
          job_title = allJobTitles[0].replace('\n','')
          if len(allJobTitles) > 1:
            for title in allJobTitles:
              if len(title) > len(job_title):
                job_title = title.replace('\n','')
          else: 
            job_title = allJobTitles[0]
        else:
          allJobTitles = None
        job_title = job_title.title()
        print("job title:  ", job_title)
        text = text.replace(job_title, "")
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        print("----------------------------------")
        text = text.replace(',\n', ', ') #Join address with ','
        lines = text.strip().split('\n') 
        address = ""
        for line in lines:
          if len(line.split(',')) > 2:
            print(line)
            address = line
            print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
            text = text.replace(line, "")
          elif len(line.split(' ')) > 3:
            text = text.replace(line, "")
        split_text = text.split( )
        name = []
        for word in split_text:
          if m.search_first_name(word):
            name.append(word)
          elif m.search_last_name(word):
            name.append(word)
        if len(name) > 1:
          name = list(dict.fromkeys(name))
        print(name)
        print("----------------------------------")
        rows = text.split("\n")
        final_name = []
        for eachName in name:
          eachRow = [row for row in rows if eachName in row]
          final_name.append(eachRow[0])
        print(most_frequent(final_name))
        if final_name:
          final_name = most_frequent(final_name)
        card_data = {
          "photo": img_name,
          "name": final_name,
          "job_title": job_title,
          "tel": telephone,
          "fax": fax,
          "website": websites,
          "email": final_emails,
          "address": address
        }
        #card_json = json.dumps(card_data)
        # outputs.append(json.loads(card_json))
        # print(json.loads(card_json))
        outputs.append(card_data)
    return outputs