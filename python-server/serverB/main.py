from fastapi import APIRouter
import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Suppress TensorFlow logging (1)
import pathlib
import tensorflow as tf
import time

# import nltk
# import spacy
# from spacy import displacy
# from collections import Counter
# import en_core_web_sm
# import matplotlib 
#-------------------------------------


# nlp = en_core_web_sm.load()

app = FastAPI()

router = APIRouter()


tf.get_logger().setLevel('ERROR')  
# Enable GPU dynamic memory allocation
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

f = open("job.txt", "r")
job_titles = []
for each in f:
    each.lower()
    job_titles.append(each.lower())
f.close()

print(job_titles[1])

MODEL_NAME = 'my_model'

PATH_TO_CKPT = os.path.join(MODEL_NAME, 'checkpoint/')
PATH_TO_CFG = os.path.join(MODEL_NAME, 'pipeline.config')

PATH_TO_LABELS = os.path.join("annotations", "label_map.pbtxt")



PATH_TO_SAVED_MODEL = os.path.join("my_model", "saved_model")
print('Loading model...', end='')
print(PATH_TO_SAVED_MODEL)
start_time = time.time()

# Load saved model and build the detection function
detect_fn = tf.saved_model.load(PATH_TO_SAVED_MODEL)

end_time = time.time()
elapsed_time = end_time - start_time
print('Done! Took {} seconds'.format(elapsed_time))

from ai import run_model

class RequestJson(BaseModel):
    original_photo_url: str

SAVE_FOLDER_PATH = "../../image_storage"

@app.post("/card/getcarddata")
async def get_card_data(requestJson: RequestJson):
    start_time = time.time()
    image_path = os.path.join(SAVE_FOLDER_PATH,requestJson.original_photo_url)
    card_data_list = run_model(detect_fn, job_titles, image_path, SAVE_FOLDER_PATH)
    end_time = time.time()
    elapsed_time = end_time - start_time
    print('Done! Took {} seconds'.format(elapsed_time))
    return card_data_list


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=5010)