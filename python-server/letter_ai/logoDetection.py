# %%
import cv2
import numpy as np
from skimage.measure import compare_ssim as ssim
from skimage.io import imread_collection as collection
import matplotlib.pyplot as plt

# %matplotlib inline


def mse(imageA, imageB):
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageB.shape[1])
    return err


def compare_image(imageA, imageB, title):
    m = mse(imageA, imageB)
    s = ssim(imageA, imageB)
    fig = plt.figure(title)
    plt.suptitle("MSE %.2f, SSIM: %.2f" % (m, s))

    # show the first image
    ax = fig.add_subplot(1, 2, 1)
    plt.imshow(imageA, cmap=plt.cm.gray)
    plt.axis("off")

    # show the second image
    ax = fig.add_subplot(1, 2, 2)
    plt.imshow(imageB, cmap=plt.cm.gray)
    plt.axis("off")

    plt.show()


hsbc_1 = cv2.imread('images/hsbc-001.jpg')
# hsbc_1 = collection('images/hsbc-001.jpg')
hsbc_2 = cv2.imread('images/hsbc-002.png')
# hsbc_2 = collection('images/hsbc-002.png')
hsbc_3 = cv2.imread('images/hsbc-003.jpg')
# hsbc_3 = collection('images/hsbc-003.jpg')
ird_1 = cv2.imread('images/ird-001.jpg')
ird_2 = cv2.imread('images/ird-002.jpg')
ird_3 = cv2.imread('images/ird-003.png')
boc_1 = cv2.imread('images/boc-001.png')
clp_1 = cv2.imread('images/clp-001.jpeg')
hkbb_1 = cv2.imread('images/hkbb-001.png')
wsd_2 = cv2.imread('images/wsd-002.jpg')

# print(hsbc_1.shape)

hsbc_1 = cv2.resize(hsbc_1, (10000, 10000))
hsbc_2 = cv2.resize(hsbc_2, (10000, 10000))
hsbc_3 = cv2.resize(hsbc_3, (10000, 10000))
ird_1 = cv2.resize(ird_1, (10000, 10000))
ird_2 = cv2.resize(ird_2, (10000, 10000))
ird_3 = cv2.resize(ird_3, (10000, 10000))
boc_1 = cv2.resize(boc_1, (10000, 10000))
clp_1 = cv2.resize(clp_1, (10000, 10000))
hkbb_1 = cv2.resize(hkbb_1, (10000, 10000))
wsd_2 = cv2.resize(wsd_2, (10000, 10000))

hsbc_1 = cv2.cvtColor(hsbc_1, cv2.COLOR_BGR2GRAY)
hsbc_2 = cv2.cvtColor(hsbc_2, cv2.COLOR_BGR2GRAY)
hsbc_3 = cv2.cvtColor(hsbc_3, cv2.COLOR_BGR2GRAY)
ird_1 = cv2.cvtColor(ird_1, cv2.COLOR_BGR2GRAY)
ird_2 = cv2.cvtColor(ird_2, cv2.COLOR_BGR2GRAY)
ird_3 = cv2.cvtColor(ird_3, cv2.COLOR_BGR2GRAY)
boc_1 = cv2.cvtColor(boc_1, cv2.COLOR_BGR2GRAY)
clp_1 = cv2.cvtColor(clp_1, cv2.COLOR_BGR2GRAY)
hkbb_1 = cv2.cvtColor(hkbb_1, cv2.COLOR_BGR2GRAY)
wsd_2 = cv2.cvtColor(wsd_2, cv2.COLOR_BGR2GRAY)

fig = plt.figure("HSBC and IRD")
images = ("1st HSBC", hsbc_1), ("2nd HSBC", hsbc_2), ("3rd HSBC", hsbc_3),
("1st IRD", ird_1), ("2nd IRD", ird_2), ("3rd IRD", ird_3),
("1st BOC", boc_1), ("1st CLP", clp_1), ("1st HKBB", hkbb_1), ("2nd WSD", wsd_2)

for (i, (name, image)) in enumerate(images):
    ax = fig.add_subplot(1, 7, i + 1)
    plt.imshow(image, cmap=plt.cm.gray)
    plt.axis("off")

plt.show()

# compare_image(hsbc_1, hsbc_1, 'exactly same logo')
# compare_image(hsbc_1, hsbc_2, 'a bit same logo a')
# compare_image(hsbc_1, hsbc_3, 'a bit same logo a')
compare_image(hsbc_2, boc_1, 'different logo')
compare_image(hsbc_2, wsd_2, 'different logo')
compare_image(hsbc_2, hkbb_1, 'different logo')
compare_image(hsbc_2, clp_1, 'different logo')
compare_image(hsbc_2, hsbc_1, 'different logo')

compare_image(hkbb_1, hsbc_3, 'different logo')
compare_image(hkbb_1, boc_1, 'different logo')
compare_image(hkbb_1, wsd_2, 'different logo')
compare_image(hkbb_1, clp_1, 'different logo')
compare_image(hkbb_1, ird_3, 'different logo')

compare_image(wsd_2, ird_3, 'different logo')
compare_image(wsd_2, hsbc_3, 'different logo')
compare_image(wsd_2, clp_1, 'different logo')

compare_image(ird_1, hsbc_2, 'different logo')
compare_image(ird_2, hsbc_3, 'different logo')
compare_image(ird_2, ird_1, 'different logo')
compare_image(ird_2, ird_3, 'different logo')
compare_image(ird_2, hsbc_2, 'different logo')


# %%
