import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from 'react-redux';
import {
    Avatar,
    Card,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    makeStyles,
} from "@material-ui/core";
import { lightBlue } from "@material-ui/core/colors";

import { IBusinessCardInfo } from "../redux/businessCard/state";
import "../css/ContactForm.css";
import { saveNewCardThunk } from "../redux/businessCard/thunk";
// import { isLoading, isLoaded } from '../redux/general/action';
import { cardListLength, isCardSaved, resetCardSavedArr } from '../redux/businessCard/action';
import { IRootState } from '../redux/store';
import { useCardImageUrl } from "../commons/imageio";

import { Button } from '@material-ui/core';
import { isLoaded } from "../redux/general/action";

const useStyles = makeStyles((theme) => ({
    rootForContactForm: {
        maxWidth: 400,
        overflow: `auto`,
        marginLeft: `auto`,
        marginRight: `auto`,
        minHeight: 345,
        backgroundColor: "#EEE",
        boxSizing: "content-box",
        margin: "12px 0 0 0",
        [theme.breakpoints.down('xs')]: {
            width: "80vw",
            height: "80vh",
        },
    },
    media: {
        height: 0,
        paddingTop: "56.25%", // 16:9
        overflowY: 'scroll'
    },
    expand: {
        transform: "rotate(0deg)",
        marginLeft: "auto",
        transition: theme.transitions.create("transform", {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: "rotate(180deg)",
    },
    avatar: {
        backgroundColor: lightBlue[500],
    },
    bCardSubmit: {
        display: "flex",
        justifyContent: "center",
        marginTop: "25px",
    },
    cardBtn: {
        backgroundColor: "#28DB9F",
        border: "1px solid #3ce8ae",
        color: "#FFFFFF",
        width: 100,
        margin: 4,
        padding: 8,
        '&:hover': {
            backgroundColor: "#3F89FF",
            // color: "#27FFBB"
        }
    },
    cardContainer: {
        marginLeft: 'auto',
        marginRight: 'auto',
        flexGrow: 1,
    },
    autoFillForm: {
        padding: "16px 0 16px 0"
    },
    autoFillCol: {
        fontSize: 14
    }

}));

const ContactForm = (props: any) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const isCardSavedArr = useSelector((state: IRootState) => state.businessCard.isCardsSavedArr)

    useEffect(() => {
        props.setIdx(props.cardArr - 1);
        dispatch(cardListLength(props.cardArr))
        dispatch(isLoaded())

        if (isCardSavedArr.length >= props.cardArr) {
            dispatch(resetCardSavedArr())
        }
    }, [props.cardArr]);


    const {
        id,
        photo,
        name,
        job_title,
        tel,
        fax,
        website,
        email,
        address,
    } = props.cardData;

    const { register, handleSubmit, setValue } = useForm<IBusinessCardInfo>({
        // resolver: yupResolver(schema),
        mode: "onBlur",
        defaultValues: {
            card_image_url: photo,
            id: id,
            fullName: name,
            jobTitle: job_title,
            tel: tel[0],
            fax: fax,
            email: email,
            website: website,
            address: address,
        },
    });

    useEffect(() => {
        setValue("card_image_url", photo)
        setValue("id", id)
        setValue("fullName", name)
        setValue("jobTitle", job_title)
        setValue("tel", tel[0])
        setValue("fax", fax)
        setValue("email", email)
        setValue("website", website)
        setValue("address", address)
    }, [id])

    const onNewCardSubmit = (data: any) => {
        if (data.fullName && data.tel) {
            let phoneArr = []
            phoneArr.push(data.tel)
            if (tel.length > 1) {
                for (let i = 0; i < tel.length - 1; i++) {
                    if (data[`extraTel${i}`] !== null) {
                        phoneArr.push(data[`extraTel${i}`] as any)
                    }
                }
            }
            const saveCardDataJson = {
                id: data.id,
                fullName: data.fullName,
                jobTitle: data.jobTitle,
                tel: phoneArr,
                fax: data.fax,
                email: data.email,
                website: data.website,
                address: data.address,
                card_image_url: data.card_image_url,
            };
            if (isCardSavedArr.length >= props.cardArr) {
                return
            } else {
                dispatch(isCardSaved(true))
            }
            dispatch(saveNewCardThunk(saveCardDataJson));

            props.setIdx((i: number) => {
                if (i <= 0) return 0;
                return i - 1;
            });
        }
    };

    const onCancelSubmit = (e: any) => {
        e.stopPropagation();
        if (isCardSavedArr.length >= props.cardArr) {
            return
        } else {
            dispatch(isCardSaved(false))
        }
        dispatch(saveNewCardThunk({}));
        props.setIdx((i: number) => {
            if (i <= 0) return 0;
            return i - 1;
        });
    }

    const imgUrlresponse = useCardImageUrl(photo).response
    return (
        <div className={classes.cardContainer}>
            <div key={`cardData_${id}`}>
                <Card className={classes.rootForContactForm}>
                    <CardHeader
                        avatar={
                            <Avatar
                                aria-label="recipe"
                                className={classes.avatar}
                            ></Avatar>
                        }
                        title={name}
                    />
                    <CardMedia
                        className={classes.media}
                        // image={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${photo}`}
                        // image={"" /* await getImageUrl(photo) */}//amazonaws
                        // image={`${process.env.REACT_APP_API_SERVER}/cardImage/${photo}`}
                        //image={"" /* await getImageUrl(photo) */}//amazonaws
                        image={imgUrlresponse?.success ? imgUrlresponse?.url : ""}
                    />
                    <form
                        onSubmit={handleSubmit(onNewCardSubmit)}
                        className="whole-contact-form"
                    >
                        <CardContent className={classes.autoFillForm}>
                            <input
                                type="hidden"
                                name="card_image_url"
                                // value={cardInfo.card_image_url}
                                ref={register}
                            />
                            <input type="hidden" name="id"
                                // value={cardInfo.id}
                                ref={register}
                            />
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">Name *</div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="text"
                                            name="fullName"
                                            className={classes.autoFillCol}
                                            // value={cardInfo.fullName}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">
                                            Job Title
                                        </div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="text"
                                            name="jobTitle"
                                            className={classes.autoFillCol}
                                            // value={cardInfo.jobTitle}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">Tel *</div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="tel"
                                            name="tel"
                                            className={classes.autoFillCol}
                                            // value={cardInfo.tel}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            {tel.length > 1 ? (
                                <>
                                    {tel.slice(1).map((eachTel: string, index: number) => (
                                        <div className="contact-column" key={`tel${index}`}>
                                            <Grid container>
                                                <Grid item xs={4} lg={4}>
                                                    <div className="contact-tag">Extra Tel</div>
                                                </Grid>
                                                <Grid item xs={8} lg={8}>
                                                    <input
                                                        type="tel"
                                                        name={`extraTel${index}`}
                                                        className={classes.autoFillCol}
                                                        value={eachTel}
                                                        ref={register}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </div>
                                    ))}
                                </>
                            ) : ""}
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">Fax</div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="tel"
                                            name="fax"
                                            className={classes.autoFillCol}
                                            // value={cardInfo.fax}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">
                                            Website
                                        </div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="text"
                                            name="website"
                                            className={classes.autoFillCol}
                                            // value={cardInfo.website}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">Email</div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="email"
                                            name="email"
                                            className={classes.autoFillCol}
                                            // value={cardInfo.email}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">
                                            Address
                                        </div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="text"
                                            name="address"
                                            className={classes.autoFillCol}
                                            // value={cardInfo.address}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className={classes.bCardSubmit}>
                                <Button
                                    type="submit"
                                    // variant="outlined"
                                    className={classes.cardBtn}
                                    name={`submitBtn_${id}`}
                                >
                                    Submit
                                </Button>
                                <Button
                                    className={classes.cardBtn}
                                    // variant="outlined"
                                    onClick={onCancelSubmit}
                                >
                                    Cancel
                                </Button>
                            </div>
                        </CardContent>
                    </form>
                </Card>
            </div>
        </div>
    );
};

export default ContactForm;
