import React from "react";
import { Route, Switch } from "react-router-dom";

import { PrivateRoute } from "./PrivateRoute";
import Home from "../pages/Home";
import Login from "../pages/Login";
import Profile from "../pages/Profile";
import Start from "../pages/NoMatch";
import TextSummarizerUploader from "./TextSummarizerUploader"
import BusinessCard from "../pages/BusinessCard";
import Letter from "../pages/Letter";
import TextSummarizer from "../pages/TextSummarizer";
import BusinessCardUploader from "./BusinessCardUploader";
import BusinessCardInfo from './BusinessCardInfo';
import LetterUploadComponent from "./LetterUpload";


const Routes: React.FC = () => {
    return (
        <Switch>
            <Route path="/" exact={true} component={Home} />
            <PrivateRoute path="/start" component={Start} />
            <PrivateRoute path="/profile" component={Profile} />
            <PrivateRoute path="/businessCard" component={BusinessCard} />
            <PrivateRoute path="/letter" component={Letter} />
            <PrivateRoute path="/textSummarizer" component={TextSummarizer} />
            <PrivateRoute path="/businessCardUploader" component={BusinessCardUploader} />
            <PrivateRoute path="/summaryUpload" component={TextSummarizerUploader} />
            <PrivateRoute path="/businessCardInfo/:id" component={BusinessCardInfo} />
            <PrivateRoute path="/letterUpload" component={LetterUploadComponent} />
            <Route path="/summaryUpload2" component={TextSummarizerUploader} />
            <Route path="/login" component={Login} />
            {/* <Route component={NoMatch} /> */}
        </Switch>
    );
};

export default Routes;
