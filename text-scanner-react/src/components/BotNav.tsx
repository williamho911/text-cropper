import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import HomeIcon from '@material-ui/icons/Home';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import DraftsIcon from '@material-ui/icons/Drafts';
import TextFieldsIcon from '@material-ui/icons/TextFields';
import SettingsIcon from '@material-ui/icons/Settings';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { push } from 'connected-react-router';
import { Drawer, Grid } from '@material-ui/core';
import { Link } from 'react-router-dom';

import { switchBotNavBarMode } from '../redux/botNavBar/action';
import CameraButton from './CameraButton';
import "../css/BotNavBar.css"
import "../css/Buttons.css"

const drawerHeight = 152

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "100vw",
            position: "fixed",
            bottom: 0,
            zIndex: 10,
            display: "flex",
        },
        drawer: {
            width: drawerHeight,
            flexShrink: 0,
            zIndex: 0,
        },
        drawerPaper: {
            height: drawerHeight,
            zIndex: 0,
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            backgroundColor: "rgba(255, 255, 255, 0.0)",
            border: "0"
        },
        iconBtn: {
            color: "#4371f0",
            border: "0 !important",
            "&:focus": {
                outline: "0 !important",
            }
        },
        drawerContainer: {
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
        },
        drawerBtn: {
            width: 48,
            height: 48,
            margin: "8px 0 0 0",
            // backgroundColor: "#4371f0",
            fontSize: 20,
            color: "#FFFFFF",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            backgroundImage: 'url(/camera-solid.svg)',
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: 48,
            '&:hover': {
                color: "#3F89FF",

            }
        },
        btnText: {
            fontSize: 10,
            color: "#28DB9F",
        }
    }),
);

export default function LabelBottomNavigation() {
    const classes = useStyles();
    const value = useSelector((state: IRootState) => state.botNavBar.label)
    const dispatch = useDispatch();
    const [cameraBtnOpen, setCameraBtnOpen] = useState(false)

    const handleDrawer = () => {
        setCameraBtnOpen((prevOpen) => !prevOpen);
    }

    const handleDrawerClose = () => {
        setCameraBtnOpen(false)
    }

    const handleChange = (event: React.ChangeEvent<{}>, newValue: string) => {
        dispatch(switchBotNavBarMode(newValue))
        const dic: any = {
            "Home": "/",
            "BusinessCard": "/businessCard",
            "Letter": "/letter",
            "TextSummarizer": "/TextSummarizer",
            "Profile": "/profile",
            "Camera": ""
        }
        const path = dic[newValue]
        if (path === "") return
        dispatch(push(path))
    };

    return (
        <div>
            <BottomNavigation value={value} onChange={handleChange} className={classes.root}>
                <BottomNavigationAction className={classes.iconBtn} label="Home" value="Home" onClick={handleDrawerClose} icon={<HomeIcon />} />
                <BottomNavigationAction className={classes.iconBtn} label="BusinessCard" value="BusinessCard" onClick={handleDrawerClose} icon={<RecentActorsIcon />} />
                <BottomNavigationAction
                    label="Camera"
                    aria-label="open drawer"
                    value="Camera"
                    icon={<CameraButton />}
                    onClick={handleDrawer}
                />
                <BottomNavigationAction className={classes.iconBtn} label="Letter" value="Letter" onClick={handleDrawerClose} icon={<DraftsIcon />} />
                <BottomNavigationAction className={classes.iconBtn} label="TextSummarizer" value="TextSummarizer" onClick={handleDrawerClose} icon={<TextFieldsIcon />} />
                {/* <BottomNavigationAction className={classes.iconBtn} label="Profile" value="Profile" onClick={handleDrawerClose} icon={<SettingsIcon />} /> */}
            </BottomNavigation>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="bottom"
                open={cameraBtnOpen}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <Grid xs={4} className={classes.drawerContainer}>
                    <Link to="/businessCardUploader" className={classes.drawerBtn} onClick={handleDrawerClose}>
                        <RecentActorsIcon />
                    </Link>
                    <span className={classes.btnText}>Business Card</span>
                </Grid>
                <Grid xs={4} className={classes.drawerContainer}>
                    <Link to="/letterUpload" className={classes.drawerBtn} onClick={handleDrawerClose}>
                        <DraftsIcon />
                    </Link>
                    <span className={classes.btnText}>Letter</span>
                </Grid>
                <Grid xs={4} className={classes.drawerContainer}>
                    <Link to="/summaryUpload" className={classes.drawerBtn} onClick={handleDrawerClose}>
                        <TextFieldsIcon />
                    </Link>
                    <span className={classes.btnText}>Text Summarizer</span>
                </Grid>
            </Drawer>
        </div>

    );
}