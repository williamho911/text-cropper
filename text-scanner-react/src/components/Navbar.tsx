import React, { useState, useRef, useEffect } from "react";
import { useSelector } from "react-redux";
import { IRootState } from '../redux/store';
import { NavLink } from "react-router-dom";
import {
    Button,
    ClickAwayListener,
    Grow,
    MenuItem,
    MenuList,
    Paper,
    Popper,
} from "@material-ui/core";

import Logout from "./Logout";
import "../css/Navbar.css";

const Navbar: React.FC = () => {
    const [open, setOpen] = useState(false);
    const [imageURL, setImageURL] = useState("");
    const anchorRef = useRef<HTMLButtonElement>(null);
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated
    );
    const username = useSelector((state: IRootState) => state.auth.user?.username)
    const profile_pic = useSelector((state: IRootState) => state.auth.user?.profile_picture_url)

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (
            anchorRef.current &&
            anchorRef.current.contains(event.target as HTMLElement)
        ) {
            return;
        }
        setOpen(false);
    };

    const handleListKeyDown = (event: React.KeyboardEvent) => {
        if (event.key === "Tab") {
            event.preventDefault();
            setOpen(false);
        }
    };

    const prevOpen = useRef(open);
    useEffect(() => {
        if (prevOpen.current === true && open === false) { }
        prevOpen.current = open;
    }, [open]);

    useEffect(() => {
        if (isAuthenticated && username && profile_pic) {
            if (profile_pic.slice(0, 5) === "https") {
                setImageURL(profile_pic);
            } else {
                const imgAPI: any =
                    process.env.REACT_APP_API_SERVER +
                    "/" +
                    profile_pic;
                setImageURL(imgAPI);
            }
        } else if (isAuthenticated && username && !profile_pic) {
            const nonImgAPI: any =
                // process.env.REACT_APP_API_SERVER + 
                "/blank-profile-pic.png";
            setImageURL(nonImgAPI);
        }
    }, [isAuthenticated, username, profile_pic]);

    return (
        <div className="Navbar__container">
            <div id="logo">
                <NavLink to="/" style={{ fontSize: "14px", font: "Roboto" }}>
                    <img className="BrandLogo" src="/head_logo.png" />
                </NavLink>
            </div>
            <ul className="Navbar__link">
                {isAuthenticated && username ? (
                    <li>
                        <Button
                            ref={anchorRef}
                            aria-controls={open ? "menu-list-grow" : undefined}
                            aria-haspopup="true"
                            onClick={handleToggle}
                            style={{
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center"
                            }}
                        >
                            <img id="profilePic" src={imageURL} alt="user profile" />
                            <div id="username"> {username}</div>
                        </Button>
                        <Popper
                            open={open}
                            placement="bottom-end"
                            anchorEl={anchorRef.current}
                            role={undefined}
                            transition
                            disablePortal
                            style={{
                                zIndex: 10
                            }}
                        >
                            {({ TransitionProps, placement }) => (
                                <Grow
                                    {...TransitionProps}
                                    style={{
                                        transformOrigin:
                                            placement === "bottom"
                                                ? "center top"
                                                : "center bottom",
                                    }}
                                >
                                    <Paper>
                                        <ClickAwayListener
                                            onClickAway={handleClose}
                                        >
                                            <MenuList
                                                autoFocusItem={open}
                                                id="menu-list-grow"
                                                onKeyDown={handleListKeyDown}
                                            >
                                                <MenuItem onClick={handleClose}>
                                                    <NavLink
                                                        className="userLink"
                                                        to="/profile"
                                                        style={{
                                                            padding: "4px 8px !important"
                                                        }}
                                                    >
                                                        PROFILE
                                                    </NavLink>
                                                </MenuItem>
                                                <MenuItem onClick={handleClose}>
                                                    <Logout />
                                                </MenuItem>
                                            </MenuList>
                                        </ClickAwayListener>
                                    </Paper>
                                </Grow>
                            )}
                        </Popper>
                    </li>
                ) : (
                        <NavLink to="/login">LOGIN</NavLink>
                    )}
            </ul>
        </div >
    );
};

export default Navbar;
