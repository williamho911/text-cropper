import React, { useRef, useState } from 'react';
import WebcamCapture from './Webcam'
import Cropper from "./Cropper"
import { useSelector, useDispatch } from 'react-redux';
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { IRootState } from '../redux/store';
import { captureImg, toggleWebcam, resetUploader, switchMode } from '../redux/uploader/action';
import { useEffect } from 'react';
import SummarizedTextArea from './SummarizedTextArea';
import Button from '@material-ui/core/Button';
import Loading from './Loading';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'center',
            minHeight: `calc(100vh - 70px - 56px)`,

            '& > *': {
                margin: 8,
                width: `calc(100% - 16px)`,
                maxWidth: 800,
                textAlign: `center`
            },
        },
    }),
);

const Uploader = () => {

    const dispatch = useDispatch();
    interface reset { fx: null | (() => any), fx2: null | ((arg0: any) => any), fx3: null | (() => any) }
    const resetTextRef = useRef<reset>({ fx: null, fx2: null, fx3: null })
    const Mode = useSelector((state: IRootState) => state.uploader.Mode)
    const ImgUploaderloadingState = useSelector((state: IRootState) => state.uploader.IsLoading);
    const webCamStatus = useSelector((state: IRootState) => state.uploader.IsWebcamOpen);
    const webCamBtnText = useSelector((state: IRootState) => state.uploader.WebCamBtuText);
    const isReturnedState = useSelector((state: IRootState) => state.uploader.IsReturnedDataState);

    useEffect(() => {
        return () => { dispatch(resetUploader()) }
    }, [dispatch]);

    const onSelectFile = (e: any) => {


        if (e.target.files && e.target.files.length > 0) {
            const reader: any = new FileReader();
            reader.addEventListener("load", () => dispatch(captureImg(reader.result)));
            reader.readAsDataURL(e.target.files[0]);
        }
    };

    const classes = useStyles();
    const [color1, setColor1] = useState<any>('#3F89FF');
    const [color2, setColor2] = useState<any>('#3F89FF');
    const [color3, setColor3] = useState<any>('#FFF');
    const [color4, setColor4] = useState<any>('#FFF');


    return (
        <div className={classes.root}>
            <Paper elevation={3} style={{ background: `transparent`, boxShadow: `none` }}>
                <div className="selectorDiv" style={{ display: "flex", justifyContent: "space-evenly" }}>
                    <Button
                        variant="contained"
                        style={{
                            margin: "8px",
                            backgroundColor: color1,
                            color: color3
                        }}
                        // color={color1}
                        onClick={() => {
                            dispatch(resetUploader());
                            dispatch(switchMode(1));
                            if (resetTextRef.current?.fx) {
                                resetTextRef.current.fx()
                            };
                            if (resetTextRef.current?.fx2) {
                                resetTextRef.current.fx2("Get summary")
                            };
                            if (resetTextRef.current?.fx3) {
                                resetTextRef.current.fx3()
                            };
                            setColor1('#27FFBB');
                            setColor2('#3F89FF');
                            setColor3('#3F89FF');
                            setColor4("#FFF")

                        }}
                    >
                        Input Text and Summarize
        </Button>
                    <Button
                        variant="contained"
                        style={{
                            margin: "8px",
                            backgroundColor: color2,
                            color: color4
                        }}
                        // color={color2}
                        onClick={() => {
                            dispatch(resetUploader());
                            dispatch(switchMode(2));
                            if (resetTextRef.current?.fx) {
                                resetTextRef.current.fx()
                            };
                            if (resetTextRef.current?.fx2) {
                                resetTextRef.current.fx2("Get summary")
                            };
                            if (resetTextRef.current?.fx3) {
                                resetTextRef.current.fx3()
                            };
                            setColor2('#27FFBB');
                            setColor1('#3F89FF');
                            setColor4('#3F89FF');
                            setColor3("#FFF")
                        }}
                    >
                        Upload Image and Summarize
        </Button>
                </div>
                {(Mode === 2) && !isReturnedState &&
                    <div>

                        {!ImgUploaderloadingState &&
                            <>
                                <div
                                    className="mode2Div"
                                    style={{
                                        display: "flex",
                                        justifyContent: "space-evenly",
                                        flexWrap: "wrap"
                                    }}
                                >
                                    <Button
                                        variant="contained"
                                        onClick={() => { dispatch(toggleWebcam()) }}
                                        style={{
                                            backgroundColor: color1,
                                            color: color3
                                        }}
                                    >
                                        {webCamBtnText}
                                    </Button>
                                    <Button
                                        variant="contained"
                                        component="label"
                                        style={{
                                            backgroundColor: color1,
                                            color: color3
                                        }}
                                    >
                                        upload
                                    <input
                                            type="file"
                                            hidden
                                            accept="image/*"
                                            onClick={() => { (!webCamStatus || dispatch(toggleWebcam())) }}
                                            onChange={onSelectFile}
                                        />
                                    </Button>
                                </div>
                                {webCamStatus &&
                                    <WebcamCapture />
                                }
                                <Cropper fetchPath="/uploadimg" />
                            </>
                        }
                        {ImgUploaderloadingState &&
                            <>
                                <Loading />
                            </>}
                    </div>}

                {((Mode === 1) || isReturnedState) && <SummarizedTextArea setRest={resetTextRef.current} />}
            </Paper>
        </div>
    );
}

export default Uploader;

