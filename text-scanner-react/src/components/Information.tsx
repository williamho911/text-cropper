import React from 'react';
import { Container, Row, Col, Jumbotron, Card, CardText, CardBody, CardLink, CardTitle, CardSubtitle, Button } from 'reactstrap';
import { Fade, Bounce, Zoom, JackInTheBox } from "react-awesome-reveal";
import "../css/Information.css"
import { Link } from 'react-router-dom';

const Information = (props: any) => {
    return (
        <Container style={{
            paddingTop: `30px`,
        }}
        >
            <Bounce>
                <Row style={{ textAlign: `center` }}>
                    <Col>
                        <Jumbotron style={{
                            maxWidth: `800px`,
                            marginLeft: `auto`,
                            marginRight: `auto`,
                            backgroundColor: "rgba(255, 255, 255, 0.6)"
                        }}>
                            <Fade delay={500}>
                                <div style={{ height: "40vh" }}>
                                    <img id="home-logo" style={{ height: "40vh" }} src="/logo.png" />
                                </div>
                            </Fade>
                            <h1 className="title">Welcome to</h1>
                            <h1 className="title-name">TEXT CROPPER</h1>
                            <p className="lead">The Free Online OCR Service made for Everyone</p>
                            <hr className="hrline" />
                            <p className="description">Use Text Cropper to capture business card information accurately, categorizes letters, and generate summary from articles</p>
                        </Jumbotron>
                    </Col>
                </Row>
            </Bounce>

            <Row style={{ maxWidth: `800px`, marginLeft: `auto`, marginRight: `auto` }}>
                <Col xs="12" sm="4" style={{ marginBottom: `10px` }}>
                    <Fade style={{ height: `100%` }}>
                        <Card style={{ height: `100%` }}>
                            <CardBody>
                                <CardTitle tag="h5">Business Card Scanner</CardTitle>
                            </CardBody>
                            <img width="100%" src="/function-01.png" alt="businessCardIcon" />
                            <CardBody>
                                <CardSubtitle tag="h6" className="mb-2 text-muted">Scan, manage and sync business cards</CardSubtitle>
                            </CardBody>
                        </Card>
                    </Fade>
                </Col>
                <Col xs="12" sm="4" style={{ marginBottom: `10px` }}>
                    <Fade style={{ height: `100%` }} delay={250}>
                        <Card style={{ height: `100%` }}>
                            <CardBody>
                                <CardTitle tag="h5">Letter Scanner</CardTitle>
                            </CardBody>
                            <img width="100%" src="/function-02.png" alt="businessCardIcon" />
                            <CardBody>
                                <CardSubtitle tag="h6" className="mb-2 text-muted">Manage and categorize your letter using Icon Detection</CardSubtitle>
                            </CardBody>
                        </Card>
                    </Fade>
                </Col>
                <Col xs="12" sm="4" style={{ marginBottom: `10px` }}>
                    <Fade style={{ height: `100%` }} delay={500}>
                        <Card style={{ height: `100%` }}>
                            <CardBody>
                                <CardTitle tag="h5">Text summarizer</CardTitle>
                            </CardBody>
                            <img width="100%" src="/function-03.png" alt="businessCardIcon" />
                            <CardBody>
                                <CardSubtitle tag="h6" className="mb-2 text-muted">Scan an article or input text and get a automatically generated summary</CardSubtitle>
                            </CardBody>
                        </Card>
                    </Fade>
                </Col>
            </Row>

            <Row style={{ textAlign: `center` }}>
                <Col>
                    <JackInTheBox>
                        <Jumbotron style={{
                            maxWidth: `800px`,
                            marginLeft: `auto`,
                            marginRight: `auto`,
                            backgroundColor: "transparent"
                        }}>
                            <h1 className="title" style={{ fontSize: `2rem`, color: `white` }}>Try Our Service Now For Free</h1>
                            <Link to="/letter">
                                <Button color="primary">Press here to start</Button>{' '}
                            </Link>
                        </Jumbotron>
                    </JackInTheBox>
                </Col>
            </Row>

        </Container>
    );
}

export default Information;