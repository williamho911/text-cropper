import React from "react";
import { useSelector } from "react-redux";
import { RouteProps, Redirect, Route } from "react-router-dom";
import { IRootState } from "../redux/store";

export function PrivateRoute({ component, ...rest }: RouteProps) {
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated
    );
    const Component = component;

    if (isAuthenticated === null) {
        return null;
    }

    if (Component == null) {
        return null;
    }

    let render: (props: any) => JSX.Element;
    if (isAuthenticated) {
        render = (props: any) => <Component {...props} />;
    } else {
        render = (props: any) => (
            <Redirect
                to={{
                    pathname: "/login",
                    state: { from: props.location },
                }}
            />
        );
    }
    return <Route {...rest} render={render} />;
}
