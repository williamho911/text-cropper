import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { IRootState } from "../redux/store";
import { logoutThunk } from "../redux/auth/thunk";

const Logout = () => {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated
    );
    const clickLogout = () => {
        dispatch(logoutThunk());
    };
    return (
        <li>
            {isAuthenticated && (
                <NavLink to="/" onClick={clickLogout} className="userLink"
                    style={{
                        padding: "4px 8px !important"
                    }}
                >
                    LOGOUT
                </NavLink>
            )}
        </li>
    );
};

export default Logout;
