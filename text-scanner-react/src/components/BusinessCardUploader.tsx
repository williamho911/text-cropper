import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../redux/store";
import {
    captureImg,
    toggleWebcam,
    resetUploader,
} from "../redux/uploader/action";
import WebcamCapture from "./Webcam";
import Cropper from "./Cropper";
import ContactForm from "./ContactForm";
import Loading from "./Loading";
import { resetCardSavedArr } from '../redux/businessCard/action';
import "../css/BusinessCard.css"
import { Button, Grid } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        btnBox: {
            width: "92vw",
            padding: "12px 0 12px",
            display: "flex",
            justifyContent: "space-around"
        },
        uploadBtn: {
            padding: "4px 12px",
            width: 160,
            backgroundColor: "#3f89ff !important",
            color: "#FFFFFF",

        }
    })
)

const BusinessCardUploader = () => {
    const classes = useStyles()
    const cardRes = useSelector(
        (state: IRootState) => state.uploader.ReturnedData
    );
    const dispatch = useDispatch();
    const ImgUploaderloadingState = useSelector(
        (state: IRootState) => state.uploader.IsLoading
    );
    const webCamStatus = useSelector(
        (state: IRootState) => state.uploader.IsWebcamOpen
    );
    const webCamBtnText = useSelector(
        (state: IRootState) => state.uploader.WebCamBtuText
    );
    const isReturnedState = useSelector(
        (state: IRootState) => state.uploader.IsReturnedDataState
    );

    const [idx, setIdx] = useState(0);
    // if (isReturnedState) {
    //     setIdx(cardRes.result?.length - 1);
    // }

    useEffect(() => {
        return () => {
            dispatch(resetUploader());
            dispatch(resetCardSavedArr())
        };
    }, [dispatch]);

    const onSelectFile = (e: any) => {
        if (e.target.files && e.target.files.length > 0) {
            const reader: any = new FileReader();
            reader.addEventListener("load", () =>
                dispatch(captureImg(reader.result))

            );
            reader.readAsDataURL(e.target.files[0]);
        }
    };


    return (
        <div className="BusinessCardUploaderContainer" style={{minHeight: `calc(100vh - 70px - 56px)`}}>
            {!isReturnedState && (
                <div>
                    {!ImgUploaderloadingState && (
                        <>
                            <div className="img-div">
                                <div className={classes.btnBox}>
                                    {/* <Grid xs={6}> */}
                                    <Button
                                        className={classes.uploadBtn}
                                        variant="contained"
                                        onClick={() => { dispatch(toggleWebcam()) }}
                                    >
                                        {webCamBtnText}
                                    </Button>
                                    {/* </Grid> */}
                                    {/* <Grid xs={6}> */}
                                    <Button
                                        className={classes.uploadBtn}
                                        variant="contained"
                                        component="label">
                                        Upload
                                        <input
                                            type="file"
                                            hidden
                                            accept="image/*"
                                            onClick={() => { (!webCamStatus || dispatch(toggleWebcam())) }}
                                            onChange={onSelectFile} />
                                    </Button>
                                    {/* </Grid> */}
                                </div>
                                {webCamStatus &&
                                    <div className="CardWebCam">
                                        <WebcamCapture />
                                    </div>
                                }
                                <Cropper fetchPath="/uploadBusinessCard" />
                            </div>
                        </>
                    )}
                    {ImgUploaderloadingState && <Loading />}
                </div>
            )}
            {isReturnedState && (cardRes !== undefined) && (
                <>
                    {/* {cardRes.result.map((cardData: any) => ( */}
                    <ContactForm
                        setIdx={setIdx}
                        cardData={cardRes.result[idx]}
                        //idx={idx}
                        cardArr={cardRes.result.length}
                    />
                    {/* ))} */}
                </>
            )}
        </div>
    );
};

export default BusinessCardUploader;
