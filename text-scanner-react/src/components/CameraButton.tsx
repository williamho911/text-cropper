import React from 'react'
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import { Icon } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';


import "../css/Buttons.css"

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        btnCircle: {
            backgroundColor: "#3f89ff",
            width: 72,
            height: 72,
            borderRadius: 36,
            cursor: "pointer",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            "&:focus": {
                outline: "0 !important",
            }
        },
        cameraBtn: {
            color: "#FFFFFF",
            fontSize: "48px",
            "&:focus": {
                outline: "0 !important",
            }
        }
    }),
);

const CameraButton = () => {
    const classes = useStyles();

    return (
        <div className={classes.btnCircle}>
            <Icon className={classes.cameraBtn} component={PhotoCameraIcon}></Icon>
        </div>
    )
}

export default CameraButton

