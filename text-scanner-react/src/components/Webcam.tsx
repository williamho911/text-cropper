import React from "react";
import Webcam from "react-webcam"
import { useDispatch, useSelector } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import { captureImg, toggleWebcam } from "../redux/uploader/action"
import { IRootState } from "../redux/store";
import '../css/Webcam.css'
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    uploadBtn: {
      padding: "4px 12px",
      width: 160,
      backgroundColor: "#3f89ff !important",
      color: "#FFFFFF",
      marginBottom: 8,
    },
    webcam: {
      border: "border: 2px solid #ffffff !important",
      marginTop: 8,
      // width: "100%",
      height: "80%",
      [theme.breakpoints.down('xs')]: {

      }
    },
  }),
);

const WebcamCapture = (props: any) => {
  const webCamStatus = useSelector((state: IRootState) => state.uploader.IsWebcamOpen)
  const classes = useStyles();
  const dispatch = useDispatch()
  const webcamRef = React.useRef<any>(null);
  const capture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    (!webCamStatus || dispatch(toggleWebcam()));
    dispatch(captureImg(imageSrc));
  }, [webcamRef, dispatch, webCamStatus]);

  return (
    <>
      <div className="webcamContainer">
        <Webcam
          className={classes.webcam}
          audio={false}
          ref={webcamRef}
          screenshotFormat="image/jpeg"
          screenshotQuality={1}
        />
        <br />
        <Button
          className={classes.uploadBtn}
          onClick={capture}
        >
          Capture photo
            </Button>
      </div>
    </>
  );
}

export default WebcamCapture;