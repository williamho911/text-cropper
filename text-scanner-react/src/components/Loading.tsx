import React from "react";
import { css } from "@emotion/react";
import HashLoader from "react-spinners/HashLoader";

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
    height: 80vh;
    text-align: center;
`;

const Loading: React.FC = () => {
    return (
        <div className="sweet-loading">
            <HashLoader css={override} size={120} color={"#123abc"} />
        </div>
    );
};

export default Loading;
