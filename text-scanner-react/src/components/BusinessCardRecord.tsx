import { Card, CardActionArea, CardContent, Typography, CardMedia, makeStyles, Button, IconButton } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getAllCardsThunk, deleteOneCardThunk } from '../redux/businessCard/thunk';

import { IRootState } from '../redux/store';
import "../css/BusinessCard.css"
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import Modal from '@material-ui/core/Modal';
import Fade from '@material-ui/core/Fade';
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import Backdrop from '@material-ui/core/Backdrop';
import Paper from '@material-ui/core/Paper';
import PhoneIcon from '@material-ui/icons/Phone';
import Popover from '@material-ui/core/Popover';
import "../css/Home.css"

const useStyles = makeStyles((theme) => ({
    nameCardRoot: {
        borderRadius: 4,
        width: '200px',
        height: "240px",
        backgroundColor: "#EEE",
        boxShadow: "1px 1px #CCC",
        margin: '20px 10px 10px 10px',
        [theme.breakpoints.down('xs')]: {
            width: '200px',
            margin: '12px 4px 4px 4px'
        },
    },
    eachCard: {
        padding: "4px"
    },
    nameCardContainer: {
        display: 'flex',
        flexWrap: "wrap",
        width: '92vw',
        justifyContent: 'space-evenly',
        alignItems: "flex-start",
        padding: '20px 10px 10px 10px',
        height: '76vh',
        overflowY: 'scroll',
        backgroundColor: "rgba(255, 255, 255, 0.88)",
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    noCardNotice: {
        padding: 12,
        textAlign: 'center',
        color: "#4371f0",
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    modal_paper: {
        overflowY: 'scroll',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[10],
        padding: theme.spacing(1, 1, 1),
        maxWidth: '80%',
        maxHeight: '80%',
    },
    modal_picture: {
        maxWidth: '100%',
        maxHeight: '100%',
    },
    typography: {
        padding: theme.spacing(0.5),
        display: 'flex',
        alignItems: 'center'
    },
    btn: {
        backgroundColor: "#3F89Ff",
        boxShadow: "1px 1px #27FFBB",
        color: "#FFFFFF",
        width: 5,
        margin: "16px 0px 16px 8px",
        "&:hover": {
            backgroundColor: "#27FFBB",
            color: "#3F89Ff"
        },
        // padding: 8,
    },
    a: {
        "&:hover": { textDecoration: 'none !important' }
    }

}));

const BusinessCardRecord: React.FC = () => {
    const classes = useStyles()
    const dispatch = useDispatch();
    const businessCardList = useSelector((state: IRootState) => state.businessCard.cardList)
    const [selectedImg, setSelectedImg] = useState(null)
    const [openModal, setOpenModal] = React.useState(false);
    const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
    const [tel, setTel] = useState("")

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;


    const handleOpenModal = () => {
        setOpenModal(true);
    };

    const handleCloseModal = () => {
        setOpenModal(false);

    };

    useEffect(() => {
        dispatch(getAllCardsThunk());
    }, [dispatch])

    // if (businessCardList?.length == 0) {

    // if (businessCardList == null || businessCardList.length === 0) {
    //     return (
    //         <div className={classes.noCardNotice}>
    //             This is no any business card record yet.
    //         </div>
    //     )
    // }

    return (
        <div className="flexCardContainer">
            <Paper className={classes.nameCardContainer}>
                {(businessCardList == null || businessCardList.length === 0) ?
                    <div className={classes.noCardNotice}>
                        There are no any records of business card yet.
                    </div>
                    :
                    <>
                        {businessCardList?.map((businessCard: any) => (
                            // <Grid key={businessCard.business_card_id} className={classes.eachCard} lg={3}>
                            <div className={classes.nameCardRoot}>
                                {/* <Link to={{
                                pathname: `/businessCardInfo/${businessCard.bCard_id}`,
                                state: businessCard
                            }}> */}
                                <CardContent>
                                    <Typography gutterBottom component="h4" >
                                        {businessCard.holder_name}
                                    </Typography>
                                </CardContent>
                                <CardActionArea onClick={() => { handleOpenModal(); setSelectedImg(businessCard.photo_url) }}>
                                    <CardMedia
                                        className={classes.media}
                                        // image={`${process.env.REACT_APP_API_SERVER}/cardImage/${businessCard.photo_url}`}
                                        image={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${businessCard.photo_url}`}
                                    />
                                </CardActionArea>
                                {/* </Link> */}
                                <div style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    paddingRight: '10px'
                                }}
                                >
                                    <div>
                                        <Link to={{
                                            pathname: `/businessCardInfo/${businessCard.bCard_id}`,
                                            state: businessCard
                                        }}>
                                            <Button className={classes.btn} size='small'>Detail</Button>
                                        </Link>
                                        <Link to={`#`} onClick={() => {
                                            if (window.confirm('Are you sure to delete this record?')) {
                                                dispatch(deleteOneCardThunk(businessCard.bCard_id))
                                            };
                                        }}>
                                            <Button className={classes.btn} size="small">Delete</Button>
                                        </Link>
                                    </div>
                                    {(businessCard.phones[0]) && (
                                        <div>
                                            <IconButton size="small" aria-describedby={id} onClick={handleClick}>
                                                <PhoneIcon style={{ color: "#3F89FF" }} onClick={() => { setTel(businessCard.phones[0]) }} />
                                            </IconButton>

                                            <Popover
                                                id={id}
                                                open={open}
                                                anchorEl={anchorEl}
                                                onClose={handleClose}
                                                anchorOrigin={{
                                                    vertical: 'bottom',
                                                    horizontal: 'center',
                                                }}
                                                transformOrigin={{
                                                    vertical: 'top',
                                                    horizontal: 'center',
                                                }}
                                            >
                                                <Typography className={classes.typography}>
                                                    <div>{tel}</div>
                                                    <IconButton>
                                                        <a href={`tel:${tel}`}>
                                                            <PhoneIcon />
                                                        </a>
                                                    </IconButton>
                                                </Typography>
                                            </Popover>
                                        </div>
                                    )}
                                </div>
                            </div>
                            /* </Grid> */
                        ))}
                    </>
                }
            </Paper>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={handleCloseModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={openModal}>
                    <div className={classes.modal_paper}>

                        <TransformWrapper wheel={{ wheelEnabled: false }} pan={{ lockAxisY: false }}>
                            <TransformComponent >

                                <img className={classes.modal_picture} src={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${selectedImg}`} ></img>
                            </TransformComponent>
                        </TransformWrapper>
                    </div>
                </Fade>
            </Modal>
        </div >
    )
}

export default BusinessCardRecord
