import { Button } from "@material-ui/core";
import React, { useState, useCallback, useRef, useEffect } from "react";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import { useDispatch, useSelector } from 'react-redux';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import { IRootState } from "../redux/store";
import { sendLetterRawImgThunk } from "../redux/uploader/thunk";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        uploadBtn: {
            padding: "4px 12px",
            width: 160,
            backgroundColor: "#28DB9F !important",
            color: "#FFFFFF",
            marginTop: 8,
        }
    }),
);

export default function LetterCropper(props: any) {
    const fetchPath = props.fetchPath
    const dispatch = useDispatch()
    const classes = useStyles();
    const ErrorMsg = useSelector((state: IRootState) => state.uploader.ErrorMsg)
    const captureImg = useSelector((state: IRootState) => state.uploader.captureImgStr)
    const [upImg, setUpImg] = useState<string>();
    const imgRef = useRef<any>(null);
    const [crop, setCrop] = useState<ReactCrop.Crop>({ unit: "%", width: 50, height: 50 });
    const [completedCrop, setCompletedCrop] = useState<ReactCrop.Crop | null>(null);


    async function getCroppedImg(crop: any) {
        let image = imgRef.current;
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = Math.ceil(crop.width * scaleX);
        canvas.height = Math.ceil(crop.height * scaleY);
        const ctx: any = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width * scaleX,
            crop.height * scaleY,
        );


        const blob: Blob | null = await new Promise(resolve => canvas.toBlob(resolve, "image/jpeg", 1))
        dispatch(sendLetterRawImgThunk(blob, fetchPath))
    }

    const onLoad = useCallback((img) => {
        imgRef.current = img;
    }, []);


    useEffect(() => {
        setUpImg(captureImg);
        setCrop({ unit: "%", width: 50, height: 50 })
    }, [captureImg]);


    return (
        <div className="CropperContainer">
            {/* <div>
                <input type="file" accept="image/*" onChange={onSelectFile} />
            </div> */}

            { upImg && <ReactCrop
                src={upImg}
                onImageLoaded={onLoad}
                crop={crop}
                onChange={(c) => { setCrop(c) }}
                onComplete={(c) => { setCompletedCrop(c) }}
            />}
            {/* <div>
                <canvas
                    ref={previewCanvasRef}
                    // Rounding is important so the canvas width and height matches/is a multiple for sharpness.
                    style={{
                        width: Math.round(completedCrop?.width ?? 0),
                        height: Math.round(completedCrop?.height ?? 0)
                    }}
                />
            </div> */}
            <br />
            {ErrorMsg && <div>{ErrorMsg}</div>}
            <Button
                style={{ marginBottom: "8px" }}
                variant="contained" type="button"
                className={classes.uploadBtn}
                disabled={!completedCrop?.width || !completedCrop?.height}
                onClick={() => {
                    // generateDownload(previewCanvasRef.current, completedCrop)
                    getCroppedImg(completedCrop)
                }
                }>Upload Image</Button>
        </div>
    );
}
