import React, { useEffect } from "react";
import "./App.css";

import { useDispatch, useSelector } from "react-redux";
import { history, IRootState } from "./redux/store";
import { ConnectedRouter } from "connected-react-router";

import Routes from "./components/Routes";
import Navbar from "./components/Navbar";
import BottomNav from "./components/BotNav";
import { restoreLoginThunk } from "./redux/auth/thunk";
import Loading from './components/Loading';
import Waves from "./components/Waves";

function App() {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated
    );
    console.log(`Test ${isAuthenticated}`);

    useEffect(() => {
        if (isAuthenticated === null) {
            // verify token
            dispatch(restoreLoginThunk());
        }
    }, [isAuthenticated, dispatch]);

    return (
        <ConnectedRouter history={history}>
            {
                isAuthenticated === null &&
                <Loading />
            }
            {
                isAuthenticated !== null && <>
                    <Waves />
                    <Navbar />
                    <Routes />
                    <BottomNav />
                </>
            }

        </ConnectedRouter>
    );
}

export default App;
