export interface ITextSummarizer {
    id: number,
    item_id: number,
    photo_url: string | null,
    OCR_text: string,
    summarized_text: string | null,
    created_at: Date
}

export interface ITextSummarizerState {
    is_deleteing: boolean,
    is_processing: boolean,
    textSummary: Array<ITextSummarizer> | null,
    msg: string
}

export const initTextSummarizerState: ITextSummarizerState = {
    is_deleteing: false,
    is_processing: false,
    textSummary: [],
    msg: ""
}
