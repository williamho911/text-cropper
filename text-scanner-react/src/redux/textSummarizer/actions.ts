import { ITextSummarizer } from "./state"

export const GET_TEXTSUMMARY_PROCESSING = "@@TEXTSUMMARIZER/GET_TEXTSUMMARY_PROCESSING"
export const GET_TEXTSUMMARY_SUCCESS = "@@TEXTSUMMARIZER/GET_TEXTSUMMARY_SUCCESS"
export const GET_TEXTSUMMARY_FAILED = "@@TEXTSUMMARIZER/GET_TEXTSUMMARY_FAILED"
export const DELETE_TEXTSUMMARY_PROCESSING = "@@TEXTSUMMARIZER/DELETE_TEXTSUMMARY_PROCESSING"
export const DELETE_TEXTSUMMARY_SUCCESS = "@@TEXTSUMMARIZER/DELETE_TEXTSUMMARY_SUCCESS"
export const DELETE_TEXTSUMMARY_FAILED = "@@TEXTSUMMARIZER/DELETE_TEXTSUMMARY_FAILED"



export function getTextSummaryProcessing() {
    return {
        type: GET_TEXTSUMMARY_PROCESSING as typeof GET_TEXTSUMMARY_PROCESSING,
    }
}

export function getTextSummarySuccess(data: Array<ITextSummarizer> | null) {
    return {
        type: GET_TEXTSUMMARY_SUCCESS as typeof GET_TEXTSUMMARY_SUCCESS,
        data
    }
}

export function getTextSummaryFailed() {
    return {
        type: GET_TEXTSUMMARY_FAILED as typeof GET_TEXTSUMMARY_FAILED
    }
}

export function deleteTextSummaryProcessing() {
    return {
        type: DELETE_TEXTSUMMARY_PROCESSING as typeof DELETE_TEXTSUMMARY_PROCESSING
    }
}

export function deleteTextSummarySuccess() {
    return {
        type: DELETE_TEXTSUMMARY_SUCCESS as typeof DELETE_TEXTSUMMARY_SUCCESS
    }
}

export function deleteTextSummaryFailed() {
    return {
        type: DELETE_TEXTSUMMARY_FAILED as typeof DELETE_TEXTSUMMARY_FAILED
    }
}

type TextSummarizerActionCreators = | typeof getTextSummaryProcessing | typeof getTextSummarySuccess | typeof getTextSummaryFailed | typeof deleteTextSummaryProcessing | typeof deleteTextSummarySuccess | typeof deleteTextSummaryFailed;
export type ITextSummarizerActions = ReturnType<TextSummarizerActionCreators>