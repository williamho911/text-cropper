import { Iletter } from "./state"

export const GET_LETTER_PROCESSING = "@@LETTERS/GET_LETTER_PROCESSING"
export const GET_LETTER_SUCCESS = "@@LETTERS/GET_LETTER_SUCCESS"
export const GET_LETTER_FAILED = "@@LETTERS/GET_LETTER_FAILED"
export const DELETE_LETTER_PROCESSING = "@@LETTERS/DELETE_LETTER_PROCESSING"
export const DELETE_LETTER_SUCCESS = "@@LETTERS/DELETE_LETTER_SUCCESS"
export const DELETE_LETTER_FAILED = "@@LETTERS/DELETE_LETTER_FAILED"

export function getLettersProcessing() {
    return {
        type: GET_LETTER_PROCESSING as typeof GET_LETTER_PROCESSING,
    }
}

export function getLettersSuccess(data: Array<Iletter> | null) {
    return {
        type: GET_LETTER_SUCCESS as typeof GET_LETTER_SUCCESS,
        data
    }
}

export function getLetterFailed() {
    return {
        type: GET_LETTER_FAILED as typeof GET_LETTER_FAILED
    }
}

export function deleteLetterProcessing() {
    return {
        type: DELETE_LETTER_PROCESSING as typeof DELETE_LETTER_PROCESSING
    }
}

export function deleteLetterSuccess() {
    return {
        type: DELETE_LETTER_SUCCESS as typeof DELETE_LETTER_SUCCESS
    }
}

export function deleteLetterFailed() {
    return {
        type: DELETE_LETTER_FAILED as typeof DELETE_LETTER_FAILED
    }
}

type LettersActionCreators = | typeof getLettersProcessing | typeof getLettersSuccess | typeof getLetterFailed | typeof deleteLetterProcessing | typeof deleteLetterSuccess | typeof deleteLetterFailed;
export type ILettersActions = ReturnType<LettersActionCreators>