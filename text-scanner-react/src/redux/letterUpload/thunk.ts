import { push } from "connected-react-router";
import { ThunkDispatch } from "../store";
import { resetLetter } from "./actions";

const { REACT_APP_API_SERVER } = process.env;


export function saveLetterThunk(data: object) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token')
        const res = await fetch(`${REACT_APP_API_SERVER}/saveletter`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({data})
        })
        const result = await res.json();

        if (res.status !== 200) {
            return
        } else {
            dispatch(resetLetter())
            dispatch(push("/letter"))
        }
    }
}