import { initProfileState } from "../profile/state"
import { ILetterUploaderState } from "./state"
import { initLetterUploadState } from "./state"
import { GET_DATA_PROCESSING, GET_DATA_SUCCESS, LetterUploaderActions, RESET_LETTER, WRITE_DATA_SUCCESS } from "./actions"

export const letterUploaderReducer = (
    state: ILetterUploaderState = initLetterUploadState,
    action: LetterUploaderActions): ILetterUploaderState => {
        switch (action.type){
            case GET_DATA_PROCESSING:
                return {
                    ...state,
                    is_processing: true,
                };
            case WRITE_DATA_SUCCESS:
                return {
                    ...state,
                    ...action.data,
                };
            case GET_DATA_SUCCESS:
                return {
                    ...state,
                    is_processing: false,
                };
            case RESET_LETTER:
                return {
                    ...state,
                    ...initLetterUploadState,
                }
            default:
                return state;
        }
    }


