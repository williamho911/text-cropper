export interface ILetterUploaderState{
    item_id: number | null,
    photo: string | null,
    organization: string | null,
    organization_id: number | null,
    is_processing: boolean
}

export const initLetterUploadState: ILetterUploaderState = {
    item_id: null,
    photo: null,
    organization: null,
    organization_id: null,
    is_processing: false,
}