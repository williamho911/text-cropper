import { IUploaderState, initialState } from "./state"
import { IUploaderActions } from './action'

export const uploaderReducers = (state: IUploaderState = initialState, action: IUploaderActions) => {
    switch (action.type) {
        case "TOGGLE_WEBCAM":
            const text = !state.IsWebcamOpen ? "Close Webcam" : "Use Webcam";
            return {
                ...state, IsWebcamOpen: !state.IsWebcamOpen, WebCamBtuText: text, captureImgStr: undefined,
            }
        case "CAPTURE_IMG":
            return {
                ...state, captureImgStr: action.img,
            }
        case "RESET_UPLOADER":
            return {
                ...initialState
            }
        case "RESET_ERROR_MSG":
            return {
                ...state, ErrorMsg: undefined, isLoading: false,
            }
        case "FAILED_TO_UPLOAD_IMG":
            console.log(action.msg)
            return {
                ...state, ErrorMsg: action.msg, isLoading: false
            }
        case "LOADING_STATE":
            return {
                ...state, IsLoading: !state.IsLoading,
            }
        case "WRITE_FETCH_DATA":
            return {
                ...state, ReturnedData: action.fetchData,
            }
        case "SWITCH_MODE":
            return {
                ...state, Mode: action.Mode
            }
        case "RETURNED_DATA_SUCCESS":
            return {
                ...state, IsReturnedDataState: true,
            }
        default:
            return state
    }
}