
export function toggleWebcam() {
    return {
        type: "TOGGLE_WEBCAM" as "TOGGLE_WEBCAM"
    }
}

export function captureImg(img: string) {
    return {
        type: "CAPTURE_IMG" as "CAPTURE_IMG",
        img
    }
}

export function resetUploader() {
    return {
        type: "RESET_UPLOADER" as "RESET_UPLOADER"
    }
}

export function failedToUpload(msg: string) {
    return {
        type: "FAILED_TO_UPLOAD_IMG" as "FAILED_TO_UPLOAD_IMG",
        msg: msg,
    }
}

export function resetErrorMsg() {
    return {
        type: "RESET_ERROR_MSG" as "RESET_ERROR_MSG"
    }
}

export function loadingState() {
    return {
        type: "LOADING_STATE" as "LOADING_STATE"
    }
}
export function switchMode(Mode: number) {
    return {
        type: "SWITCH_MODE" as "SWITCH_MODE",
        Mode
    }
}

export function writeFetchData(fetchData: any) {
    return {
        type: "WRITE_FETCH_DATA" as "WRITE_FETCH_DATA",
        fetchData
    }
}

export function returnedDataSuccess() {
    return {
        type: "RETURNED_DATA_SUCCESS" as "RETURNED_DATA_SUCCESS",
    }
}


type UploaderActionCreators = typeof returnedDataSuccess | typeof switchMode | typeof resetErrorMsg | typeof toggleWebcam | typeof captureImg | typeof resetUploader | typeof failedToUpload | typeof loadingState | typeof writeFetchData

export type IUploaderActions = ReturnType<UploaderActionCreators>