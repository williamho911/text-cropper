import { push } from "connected-react-router";
import { getDataProcessing, getDataSuccess, writeLetterData } from "../letterUpload/actions";
import { ThunkDispatch } from "../store";
import { failedToUpload, loadingState, resetErrorMsg, returnedDataSuccess, writeFetchData } from './action';
const { REACT_APP_API_SERVER } = process.env

export function sendRawImgThunk(blob: Blob | any | null, fetchPath: string) {
    return async (dispatch: ThunkDispatch) => {

        dispatch(resetErrorMsg())
        dispatch(loadingState())
        const form = new FormData()
        const filename = Date.now().toString() + ".jpeg"
        if (blob) {
            form.append("image", blob, filename)
        } else {
            dispatch(loadingState());
            dispatch(failedToUpload("Can't upload the image, please try again"));
            return
        }
        try {
            const token = localStorage.getItem('token');
            const res = await fetch(
                `${REACT_APP_API_SERVER}${fetchPath}`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                body: form
            });
            const data = await res.json();
            if (data.success) {
                dispatch(writeFetchData(data))
                dispatch(loadingState())
                dispatch(returnedDataSuccess())
            } else {
                dispatch(failedToUpload("Can't upload the image, please try again"))
                dispatch(loadingState())
            }

        } catch (error) {
            dispatch(failedToUpload("Can't upload the image, please try again"))
            dispatch(loadingState())
        }
    }
}

export function sendLetterRawImgThunk(blob: Blob | any | null, fetchPath: string) {
    return async (dispatch: ThunkDispatch) => {
        dispatch(getDataProcessing())
        dispatch(resetErrorMsg())
        dispatch(loadingState())
        const form = new FormData()
        const filename = Date.now().toString() + ".jpeg"
        if (blob) {
            form.append("image", blob, filename)
        } else {
            dispatch(loadingState());
            dispatch(failedToUpload("Can't upload the image, please try again"));
            return
        }
        try {
            const token = localStorage.getItem('token');
            const res = await fetch(
                `${REACT_APP_API_SERVER}${fetchPath}`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                body: form
            });
            const data = await res.json();
            if (data.success) {
                dispatch(writeLetterData(data.result))
                dispatch(getDataSuccess())
                dispatch(loadingState())
                dispatch(returnedDataSuccess())
            } else {
                dispatch(failedToUpload("Can't upload the image, please try again"))
                dispatch(loadingState())
            }

        } catch (error) {
            dispatch(failedToUpload("Can't upload the image, please try again"))
            dispatch(loadingState())
        }
    }
}