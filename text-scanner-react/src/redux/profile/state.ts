export interface IProfile {
    username: string | null;
    email: string | null;
    profile_picture_url: string | null;
    // password: string | null;
}


export interface IProfileState {
    content: IProfile;
    is_processing: boolean;
} 

export const initProfileState: IProfileState = {
    content: {
        username: null,
        email: null,
        profile_picture_url: null,
        // password: null,
    },
    is_processing: false
};