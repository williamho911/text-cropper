export const GET_PROFILE_PROCESSING = '@@PROFILE/GET_PROFILE_PROCESSING';
export const GET_PROFILE_SUCCESS = '@@PROFILE/GET_PROFILE_SUCCESS';
export const SAVE_NAME_SUCCESS = '@@PROFILE/SAVE_NAME_SUCCESS';
export const SAVE_NAME_PROCESSING = '@@PROFILE/SAVE_NAME_PROCESSING';

export function getProfileProcessing() {
    return {
        type: GET_PROFILE_PROCESSING as typeof GET_PROFILE_PROCESSING,
    };
}

export function getProfileSuccess() {
    return {
        type: GET_PROFILE_SUCCESS as typeof GET_PROFILE_SUCCESS,
    };
}

export function saveNameProcessing() {
    return {
        type: SAVE_NAME_PROCESSING as typeof SAVE_NAME_PROCESSING,
    }
}

export function saveNameSuccess(name: string) {
    return {
        type: SAVE_NAME_SUCCESS as typeof SAVE_NAME_SUCCESS,
        name,
    }
}

type ActionCreatorTypes = typeof getProfileProcessing | typeof getProfileSuccess | typeof saveNameProcessing | typeof saveNameSuccess;
export type ProfileActions = ReturnType<ActionCreatorTypes>;