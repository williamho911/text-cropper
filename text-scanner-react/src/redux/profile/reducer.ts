import { IProfileState, initProfileState } from "./state";
import { ProfileActions } from "./actions"
import { GET_PROFILE_PROCESSING, GET_PROFILE_SUCCESS, SAVE_NAME_PROCESSING, SAVE_NAME_SUCCESS} from "./actions"

export const profileReducer = (
    state: IProfileState = initProfileState,
    action: ProfileActions): IProfileState => {
        switch (action.type){
            case GET_PROFILE_PROCESSING:
                return {
                    ...state,
                    is_processing: true,
                };
            case GET_PROFILE_SUCCESS:
                return {
                    ...state,
                    is_processing: false,
                };
            case SAVE_NAME_PROCESSING:
                return {
                    ...state
                }
            default:
                return state;
        } 

    }