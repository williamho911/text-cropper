
const SWITCH_MODE= "@@BOTNAVBAR/SWITCH_MODE"

export function switchBotNavBarMode(label: string) {
    return {
        type: SWITCH_MODE as typeof SWITCH_MODE,
        label
    }
}



type botNavBarActionCreators =  typeof switchBotNavBarMode

export type IBotNavBarActions = ReturnType<botNavBarActionCreators>