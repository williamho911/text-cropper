export interface IBotNavBarState {
    label: string
}

export const initialState = {
    label: `Home`
}