import { push } from "connected-react-router";
import { ThunkDispatch, IRootState } from '../store';
import { getAllCards, saveNewCardFailed } from './action';
import { isLoading, isLoaded } from "../general/action";

const { REACT_APP_API_SERVER } = process.env

export function saveNewCardThunk(businessCard: any) {
    return async (dispatch: ThunkDispatch, getState: () => IRootState) => {
        dispatch(isLoading())
        const cardListLength = getState().businessCard.cardListLength
        const isCardsSavedArr = getState().businessCard.isCardsSavedArr
        if (cardListLength && isCardsSavedArr) {
            if (!isCardsSavedArr[isCardsSavedArr.length - 1]) {
            } else {
                const token = localStorage.getItem('token');
                const res = await fetch(`${REACT_APP_API_SERVER}/createNewCard`, {
                    method: 'POST',
                    headers: {
                        Authorization: `Bearer ${token}`,
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(businessCard)
                })
                const result = await res.json();
                if (res.status !== 200) {
                    dispatch(saveNewCardFailed(result.msg))
                }
                else if (isCardsSavedArr.length >= cardListLength) {
                    dispatch(push("/businessCard"))
                }
            }
            if (isCardsSavedArr.length >= cardListLength) {
                dispatch(push("/businessCard"))
            }

        }
    }
}

export function getAllCardsThunk() {
    return async (dispatch: ThunkDispatch) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/businessCards`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        });
        const result = await res.json();
        dispatch(getAllCards(result.allCards));
    }
}

export function deleteOneCardThunk(cardID: number) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${REACT_APP_API_SERVER}/businessCardID/${cardID}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        const result = await res.json();
        if (res.status !== 200) {
            dispatch(saveNewCardFailed(result.msg))
        } else {
            dispatch(getAllCardsThunk())
        }
    }
}

export function updateCardThunk(data: any) {
    return async (dispatch: ThunkDispatch, getState: () => IRootState) => {
        dispatch(isLoading())
        // const cardListLength = getState().businessCard.cardListLength
        // const isCardsSavedArr = getState().businessCard.isCardsSavedArr
        // if (cardListLength && isCardsSavedArr) {
        //     console.log(cardListLength, isCardsSavedArr)
        //     console.log("cardListArr.length - 1", isCardsSavedArr[isCardsSavedArr.length - 1])

        // if (!isCardsSavedArr[isCardsSavedArr.length - 1]) {
        //     console.log("saveNewCardThunk not run ", businessCard)
        // } else {
        const token = localStorage.getItem('token');
        const res = await fetch(`${REACT_APP_API_SERVER}/updatecard`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        const result = await res.json();
        if (res.status !== 200) {
            dispatch(saveNewCardFailed(result.msg))
        } else {
            dispatch(push("/businessCard"))
            dispatch(isLoaded())
        }
    }

    // }
}
