export interface IBusinessCardInfo {
    id: number
    fullName: string;
    jobTitle: string;
    tel: string | string[];
    fax: string;
    website: string;
    email: string;
    address: string;
    card_image_url: string;
    extraTel0?: string
    extraTel1?: string
    extraTel2?: string
    extraTel3?: string
};

export interface IBusinessCardState {
    isNewCardSaved: boolean;
    msg: string;
    newCard: IBusinessCardInfo | IBusinessCardInfo[] | null;
    cardList: IBusinessCardInfo[] | null;
    cardListLength: number | null;
    cardIndex: number;
    isCardsSavedArr: boolean[]
};

export const initBusinessCardState = {
    isNewCardSaved: false,
    msg: "",
    newCard: null,
    cardList: [],
    cardListLength: null,
    cardIndex: 0,
    isCardsSavedArr: []
};