import { IBusinessCardState, initBusinessCardState } from './state';
import { GET_ALL_CARDS, IBusinessCardActions, SAVE_NEW_CARD_FAILED, SAVE_NEW_CARD_PROCESS, SAVE_NEW_CARD_SUCCESS, CARD_LIST_LENGTH, IS_CARD_SAVED_ARR, isCardSaved, RESET_CARD_SAVED_ARR } from './action';

export const businessCardReducer = (
    state: IBusinessCardState = initBusinessCardState,
    action: IBusinessCardActions
): IBusinessCardState => {
    switch (action.type) {
        case SAVE_NEW_CARD_PROCESS:
            return {
                ...state,
                isNewCardSaved: false
            };
        case SAVE_NEW_CARD_FAILED:
            return {
                ...state,
                isNewCardSaved: false,
                msg: "",
            };
        case SAVE_NEW_CARD_SUCCESS:
            return {
                ...state,
                isNewCardSaved: true,
                msg: "",
                newCard: action.cards
            };
        case GET_ALL_CARDS:
            return {
                ...state,
                cardList: action.cardItems
            };
        case CARD_LIST_LENGTH:
            return {
                ...state,
                cardListLength: action.listLength
            };
        case IS_CARD_SAVED_ARR:
            return {
                ...state,
                isCardsSavedArr: [...state.isCardsSavedArr, action.boolean]
            };
        case RESET_CARD_SAVED_ARR:
            return {
                ...state,
                isCardsSavedArr: []
            }
        default:
            return state;
    }
};