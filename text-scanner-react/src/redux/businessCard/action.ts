import { IBusinessCardInfo } from './state';
export const SAVE_NEW_CARD_PROCESS = "@@BUSINESSCARD/SAVE_NEW_CARD_PROCESS"
export const SAVE_NEW_CARD_SUCCESS = "@@BUSINESSCARD/SAVE_NEW_CARD_SUCCESS"
export const SAVE_NEW_CARD_FAILED = "@@BUSINESSCARD/SAVE_NEW_CARD_FAILED"
export const GET_ALL_CARDS = "@@BUSINESSCARD/GET_ALL_CARDS"
export const CARD_LIST_LENGTH = "@@BUSINESSCARD/CARD_LIST_LENGTH"
export const IS_CARD_SAVED_ARR = "@@BUSINESSCARD/IS_CARD_SAVED_ARR"
export const RESET_CARD_SAVED_ARR = "@@BUSINESSCARD/RESET_CARD_SAVED_ARR"


export function saveNewCardProcess() {
    return {
        type: SAVE_NEW_CARD_PROCESS as typeof SAVE_NEW_CARD_PROCESS,
    };
};

export function saveNewCardSuccess(cards: IBusinessCardInfo | IBusinessCardInfo[]) {
    return {
        type: SAVE_NEW_CARD_SUCCESS as typeof SAVE_NEW_CARD_SUCCESS,
        cards,
    };
};

export function saveNewCardFailed(msg: string) {
    return {
        type: SAVE_NEW_CARD_FAILED as typeof SAVE_NEW_CARD_FAILED,
        msg,
    };
};

export function getAllCards(cardItems: Array<IBusinessCardInfo>) {
    return {
        type: GET_ALL_CARDS as typeof GET_ALL_CARDS,
        cardItems
    };
};

export function cardListLength(listLength: number) {
    return {
        type: CARD_LIST_LENGTH as typeof CARD_LIST_LENGTH,
        listLength
    }
}

export function isCardSaved(boolean: boolean) {
    return {
        type: IS_CARD_SAVED_ARR as typeof IS_CARD_SAVED_ARR,
        boolean
    }
}

export function resetCardSavedArr() {
    return {
        type: RESET_CARD_SAVED_ARR as typeof RESET_CARD_SAVED_ARR,
    }
}

type BusinessCardActionCreators =
    | typeof saveNewCardProcess
    | typeof saveNewCardSuccess
    | typeof saveNewCardFailed
    | typeof getAllCards
    | typeof cardListLength
    | typeof isCardSaved
    | typeof resetCardSavedArr;
export type IBusinessCardActions = ReturnType<BusinessCardActionCreators>