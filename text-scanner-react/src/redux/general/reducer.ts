import { IloadingState, initialState } from "./state";
import { IGeneralActions, LOADED, LOADING } from "./action";

export const generalReducer = (
    state: IloadingState = initialState,
    action: IGeneralActions
) => {
    switch (action.type) {
        case LOADING:
            return {
                ...state,
                isLoading: true,
            };
        case LOADED:
            return {
                ...state,
                isLoading: false,
            };
        default:
            return state;
    }
};
