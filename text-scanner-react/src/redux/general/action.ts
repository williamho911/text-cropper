export const LOADING = "@@GENERAL/LOADING";
export const LOADED = "@@GENERAL/LOADED";

export function isLoading() {
    return {
        type: LOADING as typeof LOADING,
    };
}

export function isLoaded() {
    return {
        type: LOADED as typeof LOADED,
    };
}

type GeneralActionCreators = typeof isLoading | typeof isLoaded;
export type IGeneralActions = ReturnType<GeneralActionCreators>;
