import { push } from "connected-react-router";
import { ThunkDispatch } from "../store";
import {
    loginSuccess,
    loginFailed,
    logout,
    loginProcess,
    registerFailed,
    registerSuccess,
} from "./action";

export function loginThunk(email: string, password: string) {
    return async (dispatch: ThunkDispatch) => {
        dispatch(loginProcess());
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/login`,
            {
                method: "POST",
                headers: {
                    "Content-type": "application/json",
                },
                body: JSON.stringify({ email, password }),
            }
        );

        const result = await res.json();
        if (res.status === 401) {
            dispatch(loginFailed(result.msg));
        } else {
            const user = await JSON.parse(result.user)
            localStorage.setItem("token", result.token);
            localStorage.setItem("payload", user);
            dispatch(loginSuccess(user));
            dispatch(push("/"));
        }
    };
}

export function loginFacebookThunk(accessToken: string) {
    return async (dispatch: ThunkDispatch) => {
        dispatch(loginProcess());
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/login/facebook`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ accessToken }),
            }
        );
        const result = await res.json();

        if (res.status !== 200) {
            dispatch(loginFailed(result.msg));
        } else {
            const user = await JSON.parse(result.user)
            localStorage.setItem("token", result.token);
            localStorage.setItem("payload", user);

            dispatch(loginSuccess(user));
            dispatch(push("/"));
        }
    };
}

export function loginGoogleThunk(tokenID: string) {
    return async (dispatch: ThunkDispatch) => {
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/login/google`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ tokenID }),
            }
        );
        const result = await res.json();

        if (res.status !== 200) {
            dispatch(loginFailed(result.msg));
        } else {
            const user = await JSON.parse(result.user)
            localStorage.setItem("token", result.token);
            localStorage.setItem("payload", user);

            dispatch(loginSuccess(user));
            dispatch(push("/"));
        }
    };
}

export function restoreLoginThunk() {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem("token");
        if (!token) {
            dispatch(loginFailed(""));
        }
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/userInfo`,
            {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        );
        const result = await res.json();
        if (res.status !== 200) {
            dispatch(loginFailed(result.msg));
        } else {
            dispatch(loginSuccess(result));
        }
    };
}

export function logoutThunk() {
    return async (dispatch: ThunkDispatch) => {
        dispatch(logout());
        localStorage.removeItem("payload");
        localStorage.removeItem("token");
    };
}

export function registerThunk(user: any) {
    return async (dispatch: ThunkDispatch) => {
        for (var value of user.values()) {
         }
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/register`,
            {
                method: "POST",
                // headers: {
                //     'Content-Type': 'application/json'
                // },
                body: user,
            }
        );
        const result = await res.json();
        if (res.status !== 200) {
            dispatch(registerFailed(result.msg));
        } else {
            dispatch(registerSuccess(result.result));
            dispatch(push("/"));
        }
    };
}
