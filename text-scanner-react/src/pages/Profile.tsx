import { Container } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux'
import { IRootState } from "../redux/store";
import ProfileComponent from "../components/Profile"
import { switchBotNavBarMode } from "../redux/botNavBar/action";

const Profile: React.FC = () => {
    const a = useSelector((state: IRootState) => state)
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(switchBotNavBarMode("Profile"))
    }, [])
    return (
        <Container style={{ minHeight: `calc(100vh - 70px - 56px)`, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <div className="outterflexitem" style={{ flexBasis: "500px" }}>
                <ProfileComponent />
            </div>
        </Container>
    );
};

export default Profile;
