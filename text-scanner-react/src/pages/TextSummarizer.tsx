import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom'

import CameraButton from '../components/CameraButton'
import TextSummarizerRecords from '../components/TextSummarizerRecords'
import { switchBotNavBarMode } from '../redux/botNavBar/action';

const TextSummarizer: React.FC = () => {
    const dispatch = useDispatch();
    useEffect(() => {

        dispatch(switchBotNavBarMode("TextSummarizer"))
    })
    return (
        <div>
            <div>
                <TextSummarizerRecords />

            </div>
        </div>
    )
}

export default TextSummarizer
